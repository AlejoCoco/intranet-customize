var AM = AM || {};
AM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');

    var init = function () {
        login();
        registration();
        view_modal();
        view_modal_update();
        update_registration();
        user_modal_delete();
        add_event_modal();
        addevetsdata();
        timeat();

       
        logout();
        calendarpickerbootstrap();
        modaltagingjs();
        delete_profile();
        edit_profile_modal();
        settingsnav();
        updatepersonalinformation();
        passwordchange();
        calendarpickerbootstrapbirthday();
        modal_todays_event();
        viewallcalendar();
        updateeventsdata();
        session_get();
        calendar_json();
        calendar_modal_now();
        tip_title();
        delete_calendar();
        calendar_json_demo();
    };

   var delete_calendar = function () {
        j('.delete-calendar').unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            j.cnfrm({
                title: "Confirm",
                text: "Are you sure you want to delete this post?"
            }, function () {
                j.ajax({
                    url: _url + 'inquiry/delete_calendar',
                    dataType: 'json',
                    method: 'post',
                    data: {"get_id": _this.attr('data-id')},
                    success: function (e) {
                        if (e.message === 'success') {

                            new PNotify({
                                title: 'Notification!',
                                text: 'Calendar has been successfully deleted.',
                                type: 'success'
                            });
                            j(".e_"+_this.attr('data-id')).addClass("hide");

                        }
                    }
                });
            });
            return false;
        });
    };
    var calendar_modal_now = function () {
        var _adduser = j(".calendar_view");
        var modalbox = j(".modal_calendar");
        _adduser.click(function () {
            modalbox.modal({backdrop: 'static', keyboard: false});
        });
        modalbox.find('.close').click('click', function (e) {
            e.preventDefault();
            modalbox.modal('hide');
        });
    };
    var tip_title = function () {
        j('li').tooltip();
    };


    var session_get = function () {
       
      
            
            j.ajax({
                url: _url + 'inquiry/getsession',
                method: 'post',
                dataType: 'json',
                success: function (e) {
                    if ((e.user_position === '1')  || (e.user_position === '2')) {
                        j('.eventtoday').addClass("hide");
                        console.log("adminvalidation");
                    }
                  
                }
            });
       
    };
    var settingsnav = function () {
        j('.info-tabs a').click(function (e) {
            e.preventDefault();
            j(this).tab('show');
        });
    };
    var viewallcalendar = function () {
        j('.calendar_all_button').click(function (e) {
            
            j(".calendarviewall").removeClass('hide');
            j(".calendar_all_button").addClass('hide');
            j(".calendar_hide_button").removeClass('hide');

        });
        j('.calendar_hide_button').click(function (e) { 
            j(".less").addClass('hide');
            j(".calendar_all_button").removeClass('hide');
            j(".calendar_hide_button").addClass('hide');

        });
    };
    var edit_profile_modal = function () {
        var _update = j('.modal-profile');
        j('.edit-profile').unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            _update.modal({backdrop: 'static', keyboard: false});
        });
        _update.find('.close').unbind().bind('click', function (e) {
            e.preventDefault();    
            _update.modal('hide');
        });
    };
     var modal_todays_event = function () {
        var _update = j('.modal_todays_event');
        var _trigger = j('.eventtoday');
        _trigger.unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            var _get_id = _this.attr('data-identifier');
            console.log(_get_id + " todays event");
             j.ajax({
                url: _url + 'inquiry/getcalendarinfo',
                dataType: 'json',
                method: 'post',
                data: {'getcalendarid': _get_id},
                success: function (e) {
                    var titlefield = e[0].title;
                    var datevalue = e[0].year + '-' + e[0].month + '-' + e[0].day;
                    var organizerfield = e[0].organizer;
                    var descriptionfield = e[0].description;
                    var start = e[0].start_time;
                    var end = e[0].end_time;
                    var location = e[0].location;
                    var category = e[0].category;
                    var ID = e[0].ID;
                    var year = e[0].year;
                    var month = e[0].month;
                    var day = e[0].day;
                    var concadate = year + '-' + month + '-' + day
                    j('.titleview').val(titlefield);
                    j('.descriptionfield').val(descriptionfield);
                    j('.organizerfield').val(organizerfield);
                    j('.clockpickerstart').val(start);
                    j('.clockpickerend').val(end);
                    j('.locationedit').val(location);
                    j('.categoryedit').val(category);
                    j('.date_value').val(month+"/"+day+"/"+year);
                    j('.dateid').val(ID);
                }
            });


            _update.modal({backdrop: 'static', keyboard: false});
        });
        _update.find('.close').unbind().bind('click', function (e) {
            e.preventDefault();    
            _update.modal('hide');
        });
    };
     var updateeventsdata = function () {
        j('.calendar_event_update').validate({
            rules: {
                "titleview": {required: true},
                "organizeredit": {required: true},
                "categoryedit": {required: true},
                "locationedit": {required: true},
                "descriptionfield": {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');

                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/update_event',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {
                            _html='';
                            setTimeout(function () {
                                j(".modal_calendar_event").modal('hide');
                                j('.calendar_event_create').trigger("reset");

                                new PNotify({
                                    title: 'Notification',
                                    text: "Event has been " + e.text,
                                    type: 'success'
                                });
                            }, 1000);
                       /* _html += ( '<li class="calendarviewall  '+j('#titleview').val()+'"> <a > <div class="row"> <div class="col-8"> <span class="muted" style="color:#49c4d0;font-weight:bold;"> '+j('.date_value ').val()+'</span> </div><div class="col-4 text-right" > <button class="btn btn-outline-secondary btn-sm eventtoday" data-identifier="'+j('.dateid').val()+'" ><i class="fas fa-pencil-alt"></i></button> </div></div><strong>'+j('.titleview').val()+'</strong> <span style="font-size:8px;"> By '+j('.organizerfield').val()+'</span><br/> <p> '+j('.descriptionfield').val()+' </p><span class="muted" style="color:#49c4d0;">Start at '+j('.clockpickerstart').val()+' end at '+j('.clockpickerend').val()+' </span> </a></li>');*/
                           _html = "<li>sda</li>";
                            j('.mainul').append(_html);

                        }

                    }
                });
                return false;
            }
        });
    };
    var updatepersonalinformation = function () {
        j('.acountsinfoform').validate({
            rules: {
                "fname": {required: true},
                "lname": {required: true},
                "email": {
                    required: true,
                    email: true
                },
                "birthday": {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/accountinfo',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        new PNotify({
                            title: 'Notification!',
                            text: 'Account information was succesfully updated',
                            type: 'success'
                        });
                        window.location();
                    }
                });
                return false;
            }
        });
    };

    var calendarpickerbootstrapbirthday = function () {
        j('.date2').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-70:-1"
        });
    };

    var passwordchange = function () {
        j('.passwordforminput').validate({
            rules: {
                "cpassword": {required: true},
                "rpassword": {required: true},
                "npassword": {
                    required: true,
                    equalTo: "#npassword"
                }
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/passwordchange',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {

                        if (e.message === 'currentpassword') {
                            new PNotify({
                                title: 'Notification!',
                                text: 'Current password was incorrect!',
                                type: 'alert'
                            });
                            window.location();
                        } else if (e.message === 'failed') {
                            new PNotify({
                                title: 'Notification!',
                                text: 'Password do not match',
                                type: 'alert'
                            });
                            window.location();
                        } else {
                            new PNotify({
                                title: 'Notification!',
                                text: 'Password has been updated',
                                type: 'success'
                            });
                            window.location();
                        }

                    }
                });
                return false;
            }
        });
    };
    var calendarpickerbootstrap = function () {
        j('.date1').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: "mm/dd/yy"
        });
    };
    var timeat = function () {
        j('.clockpicker').clockpicker({
            donetext: 'Done'
        });
        j('.clockpicker-button').html("select");
    };

    var logout = function () {
        var _logout = j('.logout-btn');
        _logout.click(function (e) {
            e.preventDefault();
            j.ajax({
                url: _url + 'inquiry/logout',
                method: 'post',
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'success') {
                        window.location = '';
                    }
                }
            });
        });
    };

    var delete_profile = function () {
        j('.delete-profile').unbind().bind('click', function (e) {
            e.preventDefault();
            var _this = j(this);
            j.cnfrm({
                title: "Confirm",
                text: "Are you sure you want to delete this post?"
            }, function () {
                j.ajax({
                    url: _url + 'inquiry/delete_profile',
                    dataType: 'json',
                    method: 'post',
                    data: {"get_id": _this.attr('data-id')},
                    success: function (e) {
                        if (e.message === 'success') {

                            new PNotify({
                                title: 'Notification!',
                                text: 'User Profile has been successfully deleted.',
                                type: 'success'
                            });
                            window.location();

                        }
                    }
                });
            });
            return false;
        });
    };

    var calendar_json = function(){

                j.ajax({
                    url: _url + 'inquiry/setEvents',
                    method: 'post',
                    dataType: 'json',
                    success: function (e) {
                              calendar_modal(e);
                    }
                });


    };

    var calendar_json_demo = function(){

                j.ajax({
                    url: _url + 'inquiry/setEvents',
                    method: 'post',
                    dataType: 'json',
                    success: function (e) {
                              calendar_modal_demo(e);
                    }
                });


    };



    var calendar_modal = function (_e) {
        console.log(_e);
        j('.calendar').pignoseCalendar({
           
          select: function(date, context) {
      console.log('events for this date', context.storage.schedules);
        }

           ,click: function (event, context) {
                var _this = j(this);
                var date_selected = (j(_this[0]).attr('data-date'));
                console.log(date_selected + "selected validation");
                var modalcalendarbox = j(".modal_calendar_event");
                var _html = '';
                var data = date_selected;
                var arr = data.split('-');
                var day = arr[2];
                var month = arr[1];
                var year = arr[0];

                if (month == 01) {
                    var month_caption = 'January';
                } else if (month == 02) {
                    var month_caption = 'February';
                } else if (month == 03) {
                    var month_caption = 'March';
                } else if (month == 04) {
                    var month_caption = 'April';
                } else if (month == 05) {
                    var month_caption = 'May';
                } else if (month == 06) {
                    var month_caption = 'June';
                } else if (month == 07) {
                    var month_caption = 'July';
                } else if (month == 08) {
                    var month_caption = 'August';
                } else if (month == 09) {
                    var month_caption = 'September';
                } else if (month == 10) {
                    var month_caption = 'October';
                } else if (month == 11) {
                    var month_caption = 'November';
                } else if (month == 12) {
                    var month_caption = 'December';
                }
                j(".date_caption").html(data);
                j(".input_date_value").val(month+'/'+day+'/'+year);
                j(".date_retrieve").html(month_caption);
                j(".datevalue").val(date_selected);
                modalcalendarbox.modal({backdrop: 'static', keyboard: false});
                console.log("calendar has been clicked");
                modalcalendarbox.modal("show");
                j.ajax({
                    url: _url + 'inquiry/eventbox',
                    method: 'post',
                    data: {"get_id": date_selected},
                    success: function (e) {
                        var _len = e.length;
                        if (_len === 0) {
                            _html += ("<div class='alert alert-warning'><strong>No event</strong> was set on this month!</div>");
                        } else {
                            for (var i = 0; i < _len; i++) {
                                if (e[i].title) {
                                    var desc = e[i].description.substring(0, 25) + "...";
                                    var caption = e[i].title.substring(0, 35);
                         _html += ( '<li class="calendarviewall  '+show_calendar+'"> <a > <div class="row"> <div class="col-8"> <span class="muted" style="color:#49c4d0;font-weight:bold;"> '+e[i].month+'/'+e[i].day+'/'+e[i].year+'</span> </div><div class="col-4 text-right" > <button class="buttonnocss eventtoday" data-identifier="'+e[i].ID+'" ><i class="fas fa-pencil-alt"></i></button> <button class="buttonnocss delete-calendar"  data-toggle="tooltip" title="Delete"  data-id="'+e[i].ID+'""  ><i class="far fa-trash-alt"></i></button></div></div><strong>'+e[i].title+'</strong> <span style="font-size:8px;"> By '+e[i].organizer+'</span><br/> <p> '+e[i].description+' </p><span class="muted" style="color:#49c4d0;">Start at '+e[i].start_time+' end at '+e[i].end_time+' </span> </a></li>');
                                }
                            }
                        }
                        j(".grid-row").html('').append(_html);
                        get_calendar_data();

                    }
                });
                event.preventDefault();
            }, prev: function (info, context) {
                

                 j.ajax({
                url: _url + 'inquiry/calendar_month',
                dataType: 'json',
                method: 'post',
                data: {'month_now': info.month, 'year_now': info.year},
                success: function (e) {
                    _html ='';  
                    var _len = e.length;
                    for (var i = 0; i < _len; i++) {

                            if( i <= 5){
                                    var show_calendar = "";
                            }else{
                                    var show_calendar = "hide less";
                            }


                        title = e[i].title;
                         _html += ( '<li class="calendarviewall  '+show_calendar+' e_'+e[i].ID+'"> <a > <div class="row"> <div class="col-8"> <span class="muted" style="color:#49c4d0;font-weight:bold;"> '+e[i].month+'/'+e[i].day+'/'+e[i].year+'</span> </div><div class="col-4 text-right" > <button class="buttonnocss eventtoday" data-identifier="'+e[i].ID+'" ><i class="fas fa-pencil-alt"></i></button> <button class="buttonnocss delete-calendar"  data-toggle="tooltip" title="Delete"  data-id="'+e[i].ID+'""  ><i class="far fa-trash-alt"></i></button></div></div><strong>'+e[i].title+'</strong> <span style="font-size:8px;"> By '+e[i].organizer+'</span><br/> <p> '+e[i].description+' </p><span class="muted" style="color:#49c4d0;">Start at '+e[i].start_time+' end at '+e[i].end_time+' </span> </a></li>');
                    }
                    j(".eventbox").html("<ul>"+_html+"</ul>");
                  
                     console.log(e);
                     modal_todays_event();
                     viewallcalendar();
                     session_get();
                     delete_calendar();
                }
            });
            }, next: function (info, context) {
                 j.ajax({
                url: _url + 'inquiry/calendar_month',
                dataType: 'json',
                method: 'post',
                data: {'month_now': info.month, 'year_now': info.year},
                success: function (e) {
                    _html ='';  
                    var _len = e.length;
                    for (var i = 0; i < _len; i++) {
                       if( i <= 5){
                                    var show_calendar = "";
                            }else{
                                    var show_calendar = "hide less";
                            }


                        title = e[i].title;
                         _html += ( '<li class="calendarviewall  '+show_calendar+' e_'+e[i].ID+'"> <a > <div class="row"> <div class="col-8"> <span class="muted" style="color:#49c4d0;font-weight:bold;"> '+e[i].month+'/'+e[i].day+'/'+e[i].year+'</span> </div><div class="col-4 text-right" > <button class="buttonnocss eventtoday" data-identifier="'+e[i].ID+'" ><i class="fas fa-pencil-alt"></i></button> <button class="buttonnocss delete-calendar"  data-toggle="tooltip" title="Delete"  data-id="'+e[i].ID+'""  ><i class="far fa-trash-alt"></i></button></div></div><strong>'+e[i].title+'</strong> <span style="font-size:8px;"> By '+e[i].organizer+'</span><br/> <p> '+e[i].description+' </p><span class="muted" style="color:#49c4d0;">Start at '+e[i].start_time+' end at '+e[i].end_time+' </span> </a></li>');
                    }
                    j(".eventbox").html("<ul>"+_html+"</ul>");
                    console.log(e);
                    modal_todays_event();
                    viewallcalendar();
                    session_get();
                     delete_calendar();
                }
            });
            }, scheduleOptions: {
      colors: {
         1: '#1abc9c',
         2: '#3498db',
         3: '#9b59b6',
         4: '#5c6270',
         5: '#e67e22',
         6: '#E9D460',
         7: '##c0392b',
         8: '#5c6270',
         9: '#d35400',
         10: '#bdc3c7',
         11: '#32ff7e',
         12: '#18dcff'
      }
   },
   schedules:_e,
   select: function(date, context) {
      console.log('events for this date', context.storage.schedules);
   }

        });

        j(".calendarform").click(function () {
            j(".reset").val();
            j(".addeventsform").removeClass("hide");
            j(".calendarform").addClass("hide");
            j(".calendarformcaption").removeClass("hide");
            j(".calendardetails").addClass("hide");
            j(".editcalendar").addClass("hide");

        });
        j(".calendarformcaption").click(function () {
            j(".addeventsform").addClass("hide");
            j(".calendarform").removeClass("hide");
            j(".calendarformcaption").addClass("hide");
            j(".calendardetails").removeClass("hide");
            j(".editcalendar").addClass("hide");
        });



            console.log(_e);


    };
        var calendar_modal_demo = function (_e) {
        console.log(_e);
        j('.calendardemo').pignoseCalendar({
           
         
   scheduleOptions: {
      colors: {
         1: '#1abc9c',
         2: '#3498db',
         3: '#9b59b6',
         4: '#5c6270',
         5: '#e67e22',
         6: '#E9D460',
         7: '##c0392b',
         8: '#5c6270',
         9: '#d35400',
         10: '#bdc3c7',
         11: '#32ff7e',
         12: '#18dcff'
      }
   },
   schedules:_e,select: function(date, context) {
      console.log('events for this date', context.storage.schedules);
   }

        });

        j(".calendarform").click(function () {
            j(".reset").val();
            j(".addeventsform").removeClass("hide");
            j(".calendarform").addClass("hide");
            j(".calendarformcaption").removeClass("hide");
            j(".calendardetails").addClass("hide");
            j(".editcalendar").addClass("hide");

        });
        j(".calendarformcaption").click(function () {
            j(".addeventsform").addClass("hide");
            j(".calendarform").removeClass("hide");
            j(".calendarformcaption").addClass("hide");
            j(".calendardetails").removeClass("hide");
            j(".editcalendar").addClass("hide");
        });



            console.log(_e);


    };



    var add_event_modal = function () {
        j(".addcalendar").click(function () {
            var modalcalendarbox = j(".add_event_modal");
            console.log("add event has been clicked");
            modalcalendarbox.modal("show");

        });
    };

    var execute_delete_record = function () {
        j(".delete-user-record").click(function () {
            var value_id = j(".delete_id_value").val();
            j.ajax({
                url: _url + 'inquiry/delete_employee',
                method: 'post',
                data: {"get_id": value_id},
                success: function (e) {
                    if (e.message === 'success') {
                        console.log("Deleted");
                    } else {
                        console.log("Error");
                    }
                }
            });
        });
    };
    var view_modal = function () {
        var _adduser = j(".add-user");
        var modalbox = j(".modal-view-user");
        _adduser.click(function () {
            modalbox.modal({backdrop: 'static', keyboard: false});
        });
        modalbox.find('.close').click('click', function (e) {
            e.preventDefault();
            modalbox.modal('hide');
        });
    };
    var user_modal_delete = function () {

        j(".delete-user").click(function () {
            var date_selected = j(this).attr('data-id');
            var modaldelete = j(".modal_deletet_confirmation");
            modaldelete.modal("show");
            j(".delete_id_value").val(date_selected);

        });
    };

    var submit_tags = function ($id, $data) {
        j.ajax({
            url: _url + 'inquiry/tagging',
            method: 'post',
            dataType: 'json',
            data: {"get_id": $id, "group_id": $data.item_id, "type": $data.type},
            success: function (data) {

            }
        });
    };


    var modaltagingjs = function () {
        var _mod = j('.tag-group-user');
        var _modal = j('.modal-tag');
        _mod.unbind().bind('click', function () {
            var _this = j(this);
            var _html = '';
            var _tags = '';
            _modal.modal({backdrop: 'static', keyboard: false});
            _modal.find('.id-tag-group').val(_this.attr('href'));
            j.ajax({
                url: _url + 'inquiry/gettag',
                method: 'post',
                dataType: 'json',
                data: {"postid": _this.attr('href')},
                success: function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        _html += ('<div class="tag-selector" data-bind="' + data[i].GID + '"><div class="tag-group">' + data[i].group_name + '</div></div>');
                        _tags += ('<span class="badge badge-info">' + data[i].group_name + '</span> ');
                    }
                    _modal.find('.span-tag').html(_html);
                    j('.post-tag-groups[data-tag="' + _this.attr('href') + '"]').html(_tags);
                    j('.tag-options').autoTag({
                        url: _url + 'request/tag_group_option',
                        callback: function (data) {
                            console.log(data);
                            submit_tags(_this.attr('href'), data);
                        }
                    });
                }
            });
            return false;
        });
        _modal.find('.close').unbind().bind('click', function () {
            _modal.modal('hide');
            return false;
        });
    };
    var getgroup = function (id) {
        var tag_dept = '';
        j.ajax({
            url: _url + 'inquiry/gettag',
            dataType: 'json',
            method: 'post',
            data: {'gettag': id},
            success: function (e) {
                if (e.length) {
                    var _len = e.length;
                    for (var i = 0; i < _len; i++) {
                        tag_dept += e[i].group_name;
                    }
                }
                j(".tagappend").html("<h1>" + tag_dept + "</h1>");
                j('.taggroup').val(tag_dept);
            }
        });
    };
    var dateToString = function (FromString) {
        var now = new Date(FromString * 1000);
        var dd = now.getDate();
        var mm = now.getMonth() + 1;
        var yyyy = now.getFullYear();
        var hh = now.getHours();
        var ii = now.getMinutes();
        var ss = now.getSeconds();
        if (mm < 10) {
            mm = '0' + mm;
        }
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (ii < 10) {
            ii = '0' + ii;
        }
        if (ss < 10) {
            ss = '0' + ss;
        }
        var result = (mm + "/" + dd + "/" + yyyy);
        return result;
    };
    var view_modal_update = function () {
        var trigger = j(".edit-user");
        var modalbox = j(".modal_edit_user");
        trigger.click(function () {
            var _this = j(this);
            modalbox.modal({backdrop: 'static', keyboard: false});
            calendarpickerbootstrap();
            var valueid = _this.attr('data-id');
            j.ajax({
                url: _url + 'inquiry/getusersinfo',
                dataType: 'json',
                method: 'post',
                data: {'getusersinfo': valueid},
                success: function (e) {
                    var emp_no = e[0].employee_id_no;
                    var first_name = e[0].first_name;
                    var middle_name = e[0].middle_name;
                    var last_name = e[0].last_name;
                    var gender = e[0].gender;
                    var civil_status = e[0].civil_status;

                    if (parseInt(e[0].date_of_birth) === 0) {
                        var date_of_birth = '';
                    } else {
                        var date_of_birth = dateToString(e[0].date_of_birth);
                    }
                    var contact = e[0].contact;
                    var permanent_address = e[0].permanent_address;
                    var current_address = e[0].current_address;
                    var email = e[0].email;
                    var department = e[0].department;
                    var client_name = e[0].client_name;
                    var segment = e[0].segment;
                    var department_id = e[0].department_id;
                    var location = e[0].location;
                    var site = e[0].site;
                    var job_code = e[0].job_code;
                    var job_title = e[0].job_title;
                    var job_level_grade = e[0].job_level_grade;
                    var payroll_type = e[0].payroll_type;
                    var employee_type = e[0].employee_type;
                    var employment_status = e[0].employment_status;
                    var hr_status = e[0].hr_status;

                    if (parseInt(e[0].hire_date) === 0) {
                        var hire_date = '';
                    } else {
                        var hire_date = dateToString(e[0].hire_date);
                    }

                    if (parseInt(e[0].regularization_date) === 0) {
                        var regularization_date = '';
                    } else {
                        var regularization_date = dateToString(e[0].regularization_date);
                    }

                    if (parseInt(e[0].separation_date) === 0) {
                        var seperation_date = '';
                    } else {
                        var seperation_date = dateToString(e[0].separation_date);
                    }

                    var general_reason = e[0].general_reason;
                    var specific_reason = e[0].specific_reason;
                    var voluntary_involuntary = e[0].voluntary_involuntary;
                    var biometric_id = e[0].biometric_id;
                    var reports_to_employee_id_no = e[0].reports_to_employee_id_no;
                    var second_level_supervisor = e[0].second_level_supervisor;
                    var bilability = e[0].billability;
                    var employee_remarks = e[0].employee_remarks;
                    var schedule_type = e[0].schedule_type;
                    var payroll_pre_id = e[0].payroll_pie_id;
                    var highest_education_attainment = e[0].highest_education_attainment;
                    var college_degree = e[0].college_degree;
                    var major = e[0].major;
                    var institution = e[0].institution;
                    var prior_work_expirience = e[0].prior_work_experience;
                    var previous_employer = e[0].previous_employer;
                    var type_of_industry = e[0].type_of_industry;
                    var prc_license = e[0].prc_license_no;
                    var sss_no = e[0].sss_no;
                    var tin_no = e[0].tin_no;
                    var philhealth_no = e[0].philhealth_no;
                    var pag_ibig_no = e[0].pag_ibig_no;
                    var passport_no = e[0].passport_no;
                    var tax_status = e[0].tax_status;
                    var local_trunk_line = e[0].local_trunk_line;
                    var local_trunk_line_pin = e[0].local_trunk_line_pin;
                    var skype_id = e[0].skype_id;
                    var emergency_contact_name = e[0].emergency_contact_name;
                    var emergency_contact_no = e[0].emergency_contact_no;
                    var emergency_contact_relationship = e[0].emergency_contact_relationship;
                    var emergency_contact_address = e[0].emergency_contact_address;
                    var bank_name = e[0].bank_name;
                    var bank_account_no = e[0].bank_account_no;
                    var basic_salary = e[0].basic_salary;
                    var deminimis = e[0].deminimis;
                    var transportation_allowance = e[0].transportation_allowance;
                    var travel_allowance = e[0].travel_allowance;
                    var other_allowance = e[0].other_allowance;
                    var user_level = e[0].user_level;
                    var user_status = e[0].user_status;
                    var user_role = e[0].user_role;
                    var mainid = e[0].ID;
                    j('.emp_no').val(emp_no);
                    j('.first_name').val(first_name);
                    j('.middle_name').val(middle_name);
                    j('.last_name').val(last_name);
                    j('.gender').val(gender);
                    j('.civil_status').val(civil_status);
                    j('.date_of_birth').val(date_of_birth);
                    j('.contact').val(contact);
                    j('.permanent_address').val(permanent_address);
                    j('.current_address').val(current_address);
                    j('.email').val(email);
                    j('.department').val(department);
                    j('.client_name').val(client_name);
                    j('.segment').val(segment);
                    j('.department_id').val(department_id);
                    j('.location').val(location);
                    j('.site').val(site);
                    j('.job_code').val(job_code);
                    j('.job_title').val(job_title);
                    j('.job_level_grade').val(job_level_grade);
                    j('.payroll_type').val(payroll_type);
                    j('.employee_remarks ').val(employee_remarks);
                    j('.employee_type ').val(employee_type);
                    j('.employment_status').val(employment_status);
                    j('.hr_status').val(hr_status);
                    j('.schedule_type').val(schedule_type);
                    j('.hire_date').val(hire_date);
                    j('.regularization_date').val(regularization_date);
                    j('.seperation_dates').val(seperation_date);
                    j('.general_reason').val(general_reason);
                    j('.specific_reason').val(specific_reason);
                    j('.voluntary_involuntary').val(voluntary_involuntary);
                    j('.biometric_id').val(biometric_id);
                    j('.reports_to_employee_id_no').val(reports_to_employee_id_no);
                    j('.second_level_supervisor').val(second_level_supervisor);
                    j('.type_of_industry').val(type_of_industry);
                    j('.bilability').val(bilability);
                    j('.payroll_pre_id').val(payroll_pre_id);
                    j('.highest_education_attainment').val(highest_education_attainment);
                    j('.college_degree').val(college_degree);
                    j('.major').val(major);
                    j('.institution').val(institution);
                    j('.prior_work_expirience').val(prior_work_expirience);
                    j('.previous_employer').val(previous_employer);
                    j('.prc_license').val(prc_license);
                    j('.sss_no').val(sss_no);
                    j('.tin_no').val(tin_no);
                    j('.philhealth_no').val(philhealth_no);
                    j('.pag_ibig_no').val(pag_ibig_no);
                    j('.passport_no').val(passport_no);
                    j('.tax_status').val(tax_status);
                    j('.local_trunk_line').val(local_trunk_line);
                    j('.local_trunk_line_pin').val(local_trunk_line_pin);
                    j('.skype_id').val(skype_id);
                    j('.emergency_contact_name').val(emergency_contact_name);
                    j('.emergency_contact_no').val(emergency_contact_no);
                    j('.emergency_contact_relationship').val(emergency_contact_relationship);
                    j('.emergency_contact_address').val(emergency_contact_address);
                    j('.bank_account_no').val(bank_account_no);
                    j('.basic_salary').val(basic_salary);
                    j('.deminimis').val(deminimis);
                    j('.transportation_allowance').val(transportation_allowance);
                    j('.travel_allowance').val(travel_allowance);
                    j('.other_allowance').val(other_allowance);
                    j('.user_level').val(user_level);
                    j('.user_status').val(user_status);
                    j('.user_role').val(user_role);
                    j('.mainid').val(mainid);
                    getgroup(valueid);
                }

            });
        });
        modalbox.find('.close').click('click', function (e) {
            e.preventDefault();
            modalbox.modal('hide');
        });
    };

    var update_registration = function () {
        j('.update-reg-form').validate({
            rules: {
                "employee_id_no": {required: true},
                "first_name": {required: true},
                "last_name": {required: true},
                "civil_status": {required: true},
                "date_of_birth": {required: true},
                "contact": {required: true},
                "permanent_address": {required: true},
                "email": {required: true, email: true},
                "department": {required: true},
                "employment_status": {required: true},
                "hire_date": {required: true},
                "biometric_id": {required: true},
                "reports_to_employee_id_no": {required: true},
                "tin_no": {required: true},
                "user_level": {required: true}
            }
            , showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/register',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.type === 'update') {
                            var value = j(".mainid").val();
                            var first_name = j('.first_name').val();
                            var last_name = j('.last_name').val();
                            var emp_no = j('.emp_no').val();
                            var department = j('.department').val();
                            var email = j('.email').val();
                            var location = j('.location').val();
                            j(".fname_" + value).html(first_name + " " + last_name);
                            j(".employee_no_" + value).html(emp_no);
                            j(".department_" + value).html(department);
                            j(".email" + value).html(email);
                            j(".location" + value).html(location);
                            var modalbox = j(".modal_edit_user");
                            new PNotify({
                                title: 'Notification',
                                text: first_name + " " + last_name + " has been updated!",
                                type: 'success'
                            });
                            modalbox.delay(3000).hide(0, function () {
                                modalbox.modal('hide');
                            });
  
                        } else {
                            notify('You have successfully updated the information');
                        }
                    }
                });
                return false;
            }
        });
    };
    var registration = function () {
        j('.reg-form').validate({
            rules: {
                "employee_id_no": {required: true},
                "first_name": {required: true},
                "last_name": {required: true},
                "civil_status": {required: true},
                "date_of_birth": {required: true},
                "contact": {required: true},
                "permanent_address": {required: true},
                "email": {required: true, email: true},
                "department": {required: true},
                "employment_status": {required: true},
                "hire_date": {required: true},
                "biometric_id": {required: true},
                "reports_to_employee_id_no": {required: true},
                "tin_no": {required: true},
                "user_level": {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/register',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.type === 'register') {
                            window.location = _url + "Users/page/1";
                        } else {
                            notify('You have successfully updated the information');
                        }
                    }
                });
                return false;
            }
        });
    };

    var addevetsdata = function () {
        j('.calendar_event_create').validate({
            rules: {
                "title": {required: true},
                "organizer": {required: true},
                "category": {required: true},
                "location": {required: true},
                "description": {required: true}
            }, showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'hover', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('dispose');

                });
            }, submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/addevent',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        if (e.message === 'success') {

                            setTimeout(function () {
                                j(".modal_calendar_event").modal('hide');
                                j('.calendar_event_create').trigger("reset");
                                new PNotify({
                                    title: 'Notification',
                                    text: "Event has been " + e.text,
                                    type: 'success'
                                });
                                location.reload();
                            }, 2000);
                            

                        }

                    }
                });
                return false;
            }
        });
    };
    var last_id = function () {
        j.ajax({
            url: _url + 'inquiry/get_id',
            method: 'post',
            success: function (e) {
                window.location = _url + "update/id/" + e[0]['ID'];
            }
        });
    };
    var login = function () {
        var _this = j(this);
        var button = j('.login-button');
        j('.login').validate({
            rules: {
                email_address: {
                    required: true,
                    email: true
                },
                user_password: 'required'
            }
            , showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('error-input');
                    _elm.parent().popover({content: value.message, trigger: 'hover', placement: 'top'});
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len === 0) {
                        _elm.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
                    }
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('error-input');
                    _elm.parent().popover('dispose');
                    var len = _elm.parent().find('.form-control-feedback').length;
                    if (len > 0) {
                        _elm.parent().find('.form-control-feedback').remove();
                    }
                });
            }
            , submitHandler: function (form) {
                var _this = j(form);
                j.ajax({
                    url: _url + 'inquiry/login',
                    dataType: 'json',
                    method: 'post',
                    data: _this.serialize(),
                    success: function (e) {
                        _this.find('.login-btn').addClass('btn-loading');
                        if (e.message === 'success') {
                            _this.find('.output').html('<div class="alert alert-success text-center">' + e.text + '</div>');
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        } else if (e.message === 'error') {
                            _this.find('.output').html('<div class="alert alert-warning text-center">' + e.text + '</div>');
                            _this.find('.login-btn').removeClass('btn-loading');
                        }
                    }
                });
                return false;
            }
        });

    };
    var get_calendar_data = function () {
        console.log("card has been initialize");
        j(".cardid").unbind().bind('click', function () {
            var get_id = j(this).attr("card-id");
            // j(".addeventsform").addClass("hide");
            j(".addeventsform").removeClass("hide");
            j(".calendardetails").addClass("hide");
            //j(".editcalendar").removeClass("hide");
            j(".editcalendar").addClass("hide");
            j(".calendarform").addClass("hide");
            j(".calendarformcaption").removeClass("hide");
            console.log("card has been clicked " + get_id);
            j.ajax({
                url: _url + 'inquiry/getcalendarinfo',
                dataType: 'json',
                method: 'post',
                data: {'getcalendarid': get_id},
                success: function (e) {
                    var titlefield = e[0].title;
                    var datevalue = e[0].year + '-' + e[0].month + '-' + e[0].day;
                    var organizerfield = e[0].organizer;
                    var descriptionfield = e[0].description;
                    var start = e[0].start_time;
                    var end = e[0].end_time;
                    var location = e[0].location;
                    var category = e[0].category;
                    var ID = e[0].ID;
                    var year = e[0].year;
                    var month = e[0].month;
                    var day = e[0].day;
                    var concadate = year + '-' + month + '-' + day
                    j('.title').val(titlefield);
                    j('.description').val(descriptionfield);
                    j('.organizer').val(organizerfield);
                    j('.startat').val(start);
                    j('.endat').val(end);
                    j('.location').val(location);
                    j('.category').val(category);
                    j('.idcalendar').val(ID);
                    j('.description').val(descriptionfield);
                    j('.date_caption').html(concadate);
                    j('.datevalueedit').val(concadate);
                }
            });

        });
        return false;
    };
    return {
        init: init
    };
})(jQuery);