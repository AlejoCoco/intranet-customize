(function (j) {
    j.cnfrm = function (options, callback) {
        var _body = j('body');
        var settings = j.extend({
            title: "Confirm",
            text: "Add some text"
        }, options);
        if (_body.find('.confirm-dialog').length === 0) {
            var _html = j('<div>');
            _html.addClass('confirm-dialog');
            _html.html(settings.text);
            _body.append(_html);
        }
        j('.confirm-dialog').dialog({
            title: settings.title,
            modal: true,
            buttons: { 
                "Yes": function() { 
                    j(this).dialog("close");
                    callback.call(this);
                },
                "No": function() { 
                    j(this).dialog("close");
                } 
            } 
        });
    };
})(jQuery);


