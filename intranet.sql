-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2018 at 07:52 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intranet`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `PID` int(11) NOT NULL,
  `postid` varchar(300) NOT NULL,
  `post` text NOT NULL,
  `userid` int(11) NOT NULL,
  `date` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`PID`, `postid`, `post`, `userid`, `date`) VALUES
(141, '1519168672954', '<p>Wednesday</p>\r\n\r\n<p>Partly cloudy early followed by scattered thunderstorms this afternoon. High 93F. Winds E at 5 to 10 mph. Chance of rain 40%.</p>\r\n\r\n<p>Philippines</p>\r\n', 11, '1519168734'),
(142, '1519172950149', '<p>The Outsourced Accountant is the expert in offshoring solutions for Accounting and Finance firms in Australia, the U.S.A. and New Zealand. Headquartered with an executive team in Australia, we provide dedicated team members as a high-quality offshore team. Growing rapidly with over 500+ qualified accountants and support staff in the Philippines, we deliver all the recruitment, onboarding, facilities and amazing culture to support your workflow. If you have capacity restraints, are looking to scale your firm or just want to know what is possible - call our industry specialists Cam, Shane, or Forster today on 1300 896 522, or visit www.theoutsourcedaccountant.com.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Company details</strong></p>\r\n\r\n<p>Website</p>\r\n\r\n<p><a href="http://www.theoutsourcedaccountant.com/" id="ember3676" rel="noopener noreferrer" target="_blank">http://www.theoutsourcedaccountant.com</a></p>\r\n\r\n<p><strong>Headquarters</strong></p>\r\n\r\n<p>Angeles City, Pampanga</p>\r\n\r\n<p><strong>Year founded</strong></p>\r\n\r\n<p>2013</p>\r\n\r\n<p><strong>Company type</strong></p>\r\n\r\n<p>Privately Held</p>\r\n\r\n<p><strong>Company size</strong></p>\r\n\r\n<p>501-1,000 employees</p>\r\n\r\n<p><strong>Specialties</strong></p>\r\n\r\n<p>Accounting, Bookkeeping, Outsourcing, Offshoring, Marketing, and Administration and Client Services</p>\r\n', 11, '1519172987'),
(148, '1519195347167', '<p>The Outsourced Accountant is the expert in offshoring solutions for Accounting and Finance firms in Australia, the U.S.A. and New Zealand. Headquartered with an executive team in Australia, we provide dedicated team members as a high-quality offshore team. Growing rapidly with over 500+ qualified accountants and support staff in the Philippines, we deliver all the recruitment, onboarding, facilities and amazing culture to support your workflow. If you have capacity restraints, are looking to scale your firm or just want to know what is possible - call our industry specialists Cam, Shane, or Forster today on 1300 896 522, or visit www.theoutsourcedaccountant.com.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Company details</strong></p>\r\n\r\n<p>Website</p>\r\n\r\n<p><a href="http://www.theoutsourcedaccountant.com/" id="ember3676" rel="noopener noreferrer" target="_blank">http://www.theoutsourcedaccountant.com</a></p>\r\n\r\n<p><strong>Headquarters</strong></p>\r\n\r\n<p>Angeles City, Pampanga</p>\r\n\r\n<p><strong>Year founded</strong></p>\r\n\r\n<p>2013</p>\r\n\r\n<p><strong>Company type</strong></p>\r\n\r\n<p>Privately Held</p>\r\n\r\n<p><strong>Company size</strong></p>\r\n\r\n<p>501-1,000 employees</p>\r\n\r\n<p><strong>Specialties</strong></p>\r\n\r\n<p>Accounting, Bookkeeping, Outsourcing, Offshoring, Marketing, and Administration and Client Services</p>\r\n', 11, '1519195353'),
(126, '1518130456483', '<p style="margin-left:0in; margin-right:0in">Hi All!</p>\r\n\r\n<p style="margin-left:0in; margin-right:0in">&nbsp;</p>\r\n\r\n<p style="margin-left:0in; margin-right:0in">I hope everyone is having a fantastic week and an epic start to the month!</p>\r\n', 11, '1518130484'),
(131, '1518407837326', '<p>Talented Team</p>\r\n\r\n<p>The success of TOA, our clients and our people, is built on having employees from many different backgrounds working together to deliver exceptional service to their client partners. We&rsquo;re creating the working culture of tomorrow: it&rsquo;s flexible, inclusive and globally connected. Our Certified Public Accountants (CPA) and Chartered Accountants (CA) team members gain exposure to Australian, New Zealand and USA accounting standards making them highly sought after talent within our business.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://theoutsourcedaccountant.ph/" id="ember5252" target="_blank">No weekend work! Get hired for a full-time position.</a></p>\r\n\r\n<p><a href="https://www.facebook.com/toacareers/" id="ember5254" target="_blank">Like us on facebook</a></p>\r\n', 11, '1518407856'),
(174, '1520219598711', '<p><strong>Understanding Offshoring Benefits, Risks &amp; Challenges</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>3-minute read</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>A Series on the Key Issues On Offshoring Part 1 of 6</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>There is an increasing importance in offshoring especially in this changing landscape. But as more and more firms are considering this business model in improving capacity, a number of key issues has risen, which can determine firms&rsquo; offshoring success in the future.</p>\r\n\r\n<p>While it has become easy these days, offshoring is not&nbsp;a&nbsp;recipe for success. It&rsquo;s not a magic pill for firms that want to enhance efficiency and improve capacity. Planning is crucial. And that includes identifying these key factors.</p>\r\n\r\n<p>Diverse Range Of Benefits</p>\r\n\r\n<p>For many firms, the most obvious benefit of offshoring is pure cost savings. Businesses in Australia, New Zealand, the U.S. and U.K can hire up to five employees in the Philippines for the price of one onshore.</p>\r\n\r\n<p>Offshoring provides more than this. It can help companies:</p>\r\n\r\n<ul>\r\n	<li>Operate more efficiently</li>\r\n	<li>Get access to a diverse skill set</li>\r\n	<li>Add more value to their onshore work</li>\r\n	<li>Retain employees</li>\r\n	<li>Ultimately, grow their business</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>Lower Costs And Competitive Pressures</em></strong></p>\r\n\r\n<p>Firms that have chosen the offshore route have indicated that the main motivation for adapting this business model is lower costs. Competitive pressures brought about by factors like, increased competition and smaller client and customer bases also drive companies to employ offshoring practices.</p>\r\n\r\n<p>By moving certain jobs offshore,organisations can save costs that are anywhere between one-half to one-third across a range of tasks&mdash;back and front-office tasks that can be easily commoditised.</p>\r\n\r\n<p>Now firms can either pass on these savings to their clients so they can offer more competitive prices or keep their current prices and enjoy larger margins.</p>\r\n\r\n<p><strong><em>Access To Diverse Pool Of Skilled Employees</em></strong></p>\r\n\r\n<p>Skill shortage is one of the factors that affect the current accounting landscape. In Australia, companies can definitely hire people but not those who possess the skill sets they require for specific roles.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Key Issues for Offshoring_Skill shortage" height="299" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Key-Issues-for-Offshoring_Skill-shortage-e1518680819117.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>One common scenario: you get 30 resumes for an accountant post and discover that 29 are useless. The one that you find suitable would then be twice what you&rsquo;re willing to pay for.</p>\r\n\r\n<p>This conundrum can be addressed by tapping into the large pool of offshore talents, specifically in the Philippines. Offshoring will give firms access to a more diverse pool of skilled employees so they can enjoy:</p>\r\n\r\n<ul>\r\n	<li>Specialist knowledge</li>\r\n	<li>Resources that may not be available domestically</li>\r\n	<li>Niche skills</li>\r\n	<li>Better quality of compliance work</li>\r\n	<li>Free up time for onshore employees so they can focus on client-facing tasks</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Building offshore teams will allow companies to enjoy workforce flexibility that is difficult to achieve with an in-house team.</p>\r\n\r\n<p><strong><em>Higher Value Work And Employee Retention</em></strong></p>\r\n\r\n<p>When all compliance tasks that consume way too much time are taken off the shoulders of onshore employees, these people are able to focus on more revenue-generating tasks.</p>\r\n\r\n<p>Accountants can engage more with clients and work on becoming the trusted advisor. They can better provide financial advice because they now have more time to analyse data.</p>\r\n\r\n<p>Offshoring As Part Of A Long-Term Business Strategy</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Offshoring will become easier with the help of technological advancements. Combine this with the right amount of preparation and a clear strategy, firms can enjoy the growth they aim for.</p>\r\n\r\n<ul>\r\n	<li>Local team members (and executives) need to be supportive and committed to this business model to ensure the success of any offshoring.</li>\r\n	<li>Strategic planning from the leadership team can help minimise future risks and ensure solutions to possible challenges are mapped out.</li>\r\n	<li>Onshore team members need to be informed of the purpose and process and what role they&rsquo;re going to play in the strategy.</li>\r\n	<li>When preparing to offshore, ensure that your organisation has the right systems and processes in place.</li>\r\n	<li>Onshore team must clearly map out all processes prior to offshoring to avoid future problems.</li>\r\n	<li>Implement quality control standards within the onshore team first.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Addressing Risks And Challenges</p>\r\n\r\n<p>Just like any other business strategies, offshoring naturally comes with risks and challenges especially now that the industry is constantly evolving:</p>\r\n\r\n<ul>\r\n	<li>People look for diversity in their work so those given small, repetitive administrative tasks may eventually experience burn out and eventually leave.</li>\r\n	<li>Employees want to progress in their careers in a shorter period of time.</li>\r\n	<li>Turnover is high in places where there&rsquo;s a high level of competition</li>\r\n	<li>Countries with poor regulatory frameworks can discourage clients who want to offshore.</li>\r\n	<li>Cultural differences (i.e. language barrier) can negatively impact a firm&rsquo;s offshoring strategy.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Here are some ways to deal with these challenges:</em></p>\r\n\r\n<ul>\r\n	<li>Build an inclusive culture within the company</li>\r\n	<li>Ensure high employee engagement</li>\r\n	<li>Make sure that employees can enjoy career growth</li>\r\n	<li>Allow your employees to do higher level work that will help them learn and develop new skills</li>\r\n	<li>Rotate people across tasks and processes</li>\r\n	<li>Promote mutual understanding between offshore and onshore teams through site visits</li>\r\n	<li>Offer cultural training programs to the entire team</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Conclusion</p>\r\n\r\n<p>Offshoring has tremendous benefits to firms. When done right, it can improve productivity, enhance efficiency and increase profits. But it is important to know that offshoring is not a magic pill. Firms need to invest time and effort to make it work.</p>\r\n\r\n<p>Offshoring can provide the following benefits:</p>\r\n\r\n<ul>\r\n	<li>Operate more efficiently</li>\r\n	<li>Get access to a diverse skill set</li>\r\n	<li>Add more value to their onshore work</li>\r\n	<li>Retain employees</li>\r\n	<li>Reduce costs and relieve competitive pressure</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Firms stand to yield the best results by taking note of some important tips:</p>\r\n\r\n<ul>\r\n	<li>Strategic planning from the leadership team can help minimise future risks and ensure solutions to possible challenges are mapped out.</li>\r\n	<li>Onshore team members need to be informed of the purpose and process and what role they&rsquo;re going to play in the strategy.</li>\r\n	<li>When preparing to offshore, ensure that your organisation has the right systems and processes in place.</li>\r\n	<li>Onshore team must clearly map out all processes prior to offshoring to avoid future problems.</li>\r\n	<li>Implement quality control standards within the onshore team first.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This business strategy comes with some risks and challenges:</p>\r\n\r\n<ul>\r\n	<li>Those who want more diversity in their work can look for another job</li>\r\n	<li>People are looking forward to advancing in their careers</li>\r\n	<li>High levels of competition in various places can impact employee turnover</li>\r\n	<li>Firms may be discouraged by countries with poor regulatory frameworks.</li>\r\n	<li>Cultural differences (i.e. language barrier) can negatively impact a firm&rsquo;s offshoring strategy.</li>\r\n</ul>\r\n', 11, '1520219699'),
(136, '1518994788971', '<p>MWW</p>\r\n', 11, '1518994818'),
(137, '1519108842876', '<p>6</p>\r\n', 11, '1519108898'),
(138, '1519113122745', '<p>bura</p>\r\n\r\n<p>&nbsp;</p>\r\n', 11, '1519113175'),
(139, '1519168024431', '<p>emerson</p>\r\n', 11, '1519168070'),
(152, '1519703945584', '<p><strong>Drive Six Figure+ Revenue To Your Firm In 2018</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Speakers:</p>\r\n\r\n<p>Nick Sinclair, Founder, The Outsourced Accountant</p>\r\n\r\n<p>Jack Blayney, Account Management Team Lead, Receipt Bank</p>\r\n\r\n<p>The Outsourced Accountant is committed to improving the accounting practice. As part of our Growth series, we aim to fulfill several objectives:</p>\r\n\r\n<ul>\r\n	<li>Link technology and people</li>\r\n	<li>Make specific processes more automated</li>\r\n	<li>Have the right people do the right work</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The webinar&rsquo;s main thrust is to provide expert insights on how bookkeeping and improved accounting practices can address key issues plaguing firms.</p>\r\n\r\n<p>The Outsourced Accountant is focused on growth as a direct response to feedback from our clients and our target market. The insights obtained from these responses are then used to support accountants so they can improve their practice, scale the firm and, hopefully, improve work-life balance and their overall quality of life.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&ldquo;If you aren&rsquo;t set up for the major changes that are happening, like the privacy legislation, then it may be too late by the time these things are implemented.&rdquo;</p>\r\n\r\n<p>Nick Sinclair, CEO of The Outsourced Accountant</p>\r\n\r\n<p>Topics covered in this webinar include:</p>\r\n\r\n<ul>\r\n	<li>Key issues affecting accountants</li>\r\n	<li>Automation and its impact on bookkeeping</li>\r\n	<li>What leading firms are doing</li>\r\n	<li>Future of automation</li>\r\n	<li>Bookkeeping structure (traditional and cloud-based)</li>\r\n	<li>Building a bookkeeping division</li>\r\n</ul>\r\n\r\n<p>What Are The Key Issues Affecting Accountants?</p>\r\n\r\n<p><strong>They are crushed with workflow and capacity</strong></p>\r\n\r\n<p>Accountants are busiest during this time of the year because we&rsquo;re a few months away from tax season. That means more work. Add that to the compliance work that bogs them down on a regular basis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Key Issues" height="315" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Key-Issues-e1517557960162.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Accountants are neck-deep in emails and calls, according to data we&rsquo;ve acquired. Add that to other issues like meetings, calendar management, reporting and handling client issues, and you&rsquo;re left with time-strapped accountants who can&rsquo;t focus on client-facing activities.</p>\r\n\r\n<p>We&rsquo;ve uncovered the three reasons why clients leave:</p>\r\n\r\n<ul>\r\n	<li><strong>Reactive advice</strong>. Firms don&rsquo;t have their workflow under control and that results to accountants always &ldquo;being too busy&rdquo; they don&rsquo;t get to talk to their clients. They avoid ringing up clients in fear of getting additional work.</li>\r\n	<li><strong>Poor responsiveness</strong>. Compliance work can make accountants too busy to respond to client&rsquo;s enquiries, which is not good because clients demand more; they want real-time accountants who can address their concerns when they need it.</li>\r\n	<li><strong>Trusted referral from a competitor</strong>. Today, due to technology, clients can easily switch accountants if they feel their demands are not being met. A competitor who&rsquo;s doing better in terms of proactiveness and efficiency.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Important: 89% of businesses consider client experience to be a primary differentiator. Are you actually delivering something that differentiates you from your competitor?</em></p>\r\n\r\n<p><em>Watch:&nbsp;<a href="https://www.youtube.com/watch?v=ibCLnuEcgmU">Nick on being proactive and not reactive</a>.</em></p>\r\n\r\n<p><em>Read:&nbsp;<a href="http://theoutsourcedaccountant.com/blog/7-actionable-ways-to-become-a-real-time-accountant/">How to become a real-time accountant</a>.&nbsp;</em></p>\r\n\r\n<p><em>Watch:&nbsp;<a href="https://www.youtube.com/watch?v=1MKJDwNuY3s">Changing customer expectations</a>.&nbsp;</em></p>\r\n\r\n<p><em>Watch:&nbsp;<a href="https://www.youtube.com/watch?v=1uAP7hAK-fg">Are You Always Available To Clients?</a>&nbsp;</em></p>\r\n\r\n<p>Our findings show that most accountants are sitting in front of their computers responding to emails and calls and issues, and they are not actually doing work that is based on their charge out rate.&nbsp;<em>They are not adding value to their clients</em>.</p>\r\n\r\n<p><strong>Accountants have really low contact time</strong></p>\r\n\r\n<p>Accountants spend too much time on compliance. Low contact time leads to inefficiencies. Compliance work can produce a domino effect, which ultimately results to accountants being unable to add value for their clients.</p>\r\n\r\n<p>We found that partners and firms are so swamped with work they no longer have time to do client-facing tasks. These tasks are what actually drive profit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Low Contact Time" height="315" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Low-Contact-Time-e1517558184696.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Accountancy firms are spending a significant amount of time just reconciling transactions. They are occupied with managing and processing the finances and chasing clients for paperwork.</p>\r\n\r\n<p>These figures show that firms are stuck with compliance work instead of client-facing activities. We aim to implement processes that take the burden of compliance off the accountants so they can pay more attention to adding value.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&ldquo;Compliance is really necessary but it&rsquo;s not sexy.&rdquo;</p>\r\n\r\n<p>Jack Blayney</p>\r\n\r\n<p><strong>Technology overload</strong></p>\r\n\r\n<p>There is plenty of technology available for accountants that can help them automate their tasks and, eventually, strip off significant amount of time spent on compliance.</p>\r\n\r\n<p>The problem is many firms are swamped with too many software it&rsquo;s unclear how their IT stack can help them. There are hundreds of them out there it&rsquo;s become overwhelming.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Technology Overload" height="314" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Technology-Overload-e1517558261353.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Firms need to take a look at their current IT stack and identify how they&rsquo;re actually utilising it.</p>\r\n\r\n<p>Based on our findings:</p>\r\n\r\n<ul>\r\n	<li>25% to 30% are overwhelmed by their IT stack</li>\r\n	<li>36% said they know what they need but haven&rsquo;t implemented them yet</li>\r\n	<li>10% said they have a seamless IT stack</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Firms that can successfully identify these key issues and find ways to go around them stand to benefit from the following:</p>\r\n\r\n<ul>\r\n	<li>Reduced partner hours</li>\r\n	<li>Improved revenues</li>\r\n	<li>Enhanced efficiencies</li>\r\n	<li>Improved cash flow</li>\r\n	<li>Additional services offered (business advisory)</li>\r\n	<li>Increased referrals</li>\r\n	<li>Improved staff engagement and retention</li>\r\n</ul>\r\n\r\n<p>Bookkeeping And Automation</p>\r\n\r\n<p>Bookkeeping has evolved from being a mere transaction reconciliation process to a valuable core service that&rsquo;s used to provide vital insights to financial advisory. &nbsp;</p>\r\n\r\n<p>Key takeaways:</p>\r\n\r\n<ul>\r\n	<li>Bookkeeping is the first part of a process and firms need to own it from start to finish</li>\r\n	<li>Cloud technology has made the transition a lot easier</li>\r\n	<li>Technology has made traditional reactive accounting services more proactive</li>\r\n	<li>Both parties can see client data in real-time</li>\r\n	<li>Accountants can make forecasts with pinpoint accuracy</li>\r\n	<li>Firms can easily transition clients to a mobile and paperless process</li>\r\n	<li>Bookkeeping comes before accounting and, now, more firms are adopting it because it makes the end-of-year work faster and easier</li>\r\n	<li>Firms that add bookkeeping to their services</li>\r\n</ul>\r\n\r\n<p>What Do The Leading Firms Do?</p>\r\n\r\n<ul>\r\n	<li>CEOs act as leaders and are not on the tools</li>\r\n	<li>Promote work-life balance</li>\r\n	<li>Have effectively mapped out their people strategy</li>\r\n	<li>The process, IT stack and people are all sorted properly</li>\r\n	<li>Enjoy three to six times revenue on their staff unlike others that only get twice the revenue for a higher cost per employee</li>\r\n	<li>The right people are doing the right job at the right costs</li>\r\n	<li>Accountants have enough time to spend with clients</li>\r\n	<li>Real-time access to data for both parties</li>\r\n	<li>High staff engagement</li>\r\n	<li>Increasing NPS scores and inbound referrals</li>\r\n	<li>Low client attrition rate</li>\r\n	<li>Increased profits</li>\r\n</ul>\r\n\r\n<p>The Future Of Automation</p>\r\n\r\n<ul>\r\n	<li>Robots cannot replace accountants.</li>\r\n	<li>Simple, repetitive tasks can be automated for improved productivity.</li>\r\n	<li>Accountants will always be required for face-to-face contact with clients.</li>\r\n	<li>Receipt Bank aims to make their partners custodians of data: responsible for ensuring data integrity, improving efficiency and automation.</li>\r\n</ul>\r\n\r\n<p>The Bookkeeping Structure</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Bookkeeping Structure" height="316" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Bookkeeping-Structure-1133x555.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>There is not much different between workflows of the traditional bookkeeping workflow and cloud bookkeeping workflow. The same things need to be done. The difference, however, is in the manner in which it is done.</p>\r\n\r\n<p><em>What happens when you move to cloud bookkeeping?</em></p>\r\n\r\n<ul>\r\n	<li>Information becomes digital</li>\r\n	<li>Technology will automate manual tasks</li>\r\n	<li>Firms are able to create smoother workflows</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>The Outsourced Accountant Case Study</em></strong></p>\r\n\r\n<p>We conducted a survey and found that less than 20% of our clients are actually using Receipt Bank. This is concerning because it means these firms are not maximising the staff they have, which also means they are not supercharging their businesses.</p>\r\n\r\n<p><em>What are the accountants&rsquo; concerns with automation?</em></p>\r\n\r\n<p>Accountants and bookkeepers that charge per hour are worried that technology will get the work done quicker, which means their hours are reduced. Reduced hours mean reduced income.</p>\r\n\r\n<p>These days, the bookkeeping ratio (number of clients per bookkeeper) is low. This can be traced back to not using cloud technology.</p>\r\n\r\n<ul>\r\n	<li>It&rsquo;s currently around 1:10 million</li>\r\n	<li>Receipt Bank partners are aiming for 1:30</li>\r\n	<li>The best so far is 1:52</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>As processes become more efficient, with the help of tools like Receipt Bank, we want the number to start rising because that will be a key indicator of growth: more clients and more revenue.</p>\r\n\r\n<p>Technology enables accounting and bookkeeping firms to really add value at a higher scale for clients. This will mean the firm will become extremely profitable because they have extremely satisfied clients.</p>\r\n\r\n<p>A couple of key takeaways:</p>\r\n\r\n<ul>\r\n	<li>Have an offshore team complement your local efforts</li>\r\n	<li>Combine technology and people</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Assessing your proactiveness with clients</em></p>\r\n\r\n<p>We ran a poll during the webinar to determine a firm&rsquo;s proactiveness. The results showed that approximately 80% of the attendees are highly reactive.</p>\r\n\r\n<p>This is a result of issues with workflow and capacity.</p>\r\n\r\n<p>Identifying The Trusted Advisor</p>\r\n\r\n<p>Accountants should be the trusted advisor. However, in the current landscape, the bookkeepers are emerging as the ones clients trust most. Here are the reasons:</p>\r\n\r\n<ul>\r\n	<li>Bookkeepers have access to client data 24/7</li>\r\n	<li>Bookkeepers are frequently in touch with clients</li>\r\n	<li>Bookkeepers acquire valuable data and insights</li>\r\n	<li>Bookkeepers are starting to provide management reporting</li>\r\n	<li>Bookkeepers are embracing technology and using it to move to a more advisory-style business</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>These present an opportunity for accounting firms:&nbsp;<em>start bringing in bookkeeping services in-house</em>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Trusted Advisor" height="315" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Trusted-Advisor-e1517558846854.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Benefits of having your own bookkeeping division:</p>\r\n\r\n<ul>\r\n	<li>You can further enhance the trust with clients</li>\r\n	<li>You can control the relationship because you&rsquo;re doing both the bookkeeping and accounting</li>\r\n	<li>Clients will have no reason to leave</li>\r\n</ul>\r\n\r\n<p>Building Your Own Bookkeeping Division</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="Drive Six Figures Revenue To Your Firm in 2018 Bookkeeping Divsion" height="316" src="http://theoutsourcedaccountant.com/wp-content/uploads/2018/02/Feb-Webinar-Bookkeeping-Divsion-e1517558698118.jpg" width="560" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Utilise technology and what you already have from your client</li>\r\n	<li>The average fee per person generated is really low</li>\r\n	<li>You can save 30% to 50% of time on annual tax and compliance work</li>\r\n	<li>Bookkeeping revenue is always twice or thrice more than what accountants get for compliance work</li>\r\n	<li>With the help of technology, you can get 30+ clients per employee</li>\r\n	<li>You can use it as an effective lead generation method</li>\r\n	<li>You can generate a revenue of five to ten thousand per bookkeeping client</li>\r\n	<li>Having a bookkeeping division will strengthen and&nbsp;<a href="http://theoutsourcedaccountant.com/blog/improve-client-experience-with-these-productivity-tips/">improve client relationship</a></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Conclusion</p>\r\n\r\n<p>To enhance workflow, improve efficiency and drive revenue to your firm, it&rsquo;s important to consider important factors:</p>\r\n\r\n<ul>\r\n	<li>Key issues that affect accountants</li>\r\n	<li>Technology and its impact on the accounting practice</li>\r\n	<li>Traditional bookkeeping and cloud-based bookkeeping</li>\r\n</ul>\r\n\r\n<p>Understand that clients leave for various reasons:</p>\r\n\r\n<ul>\r\n	<li>Reactive advice from accountants</li>\r\n	<li>Poor responsiveness</li>\r\n	<li>Trusted referral from a competitor</li>\r\n</ul>\r\n\r\n<p>Bookkeeping has already evolved to become a core accounting practice that drives revenue. And there&rsquo;s no reason for accountants to fear automation because robots cannot replace accountants. Accountants will always be required for face-to-face client interaction.</p>\r\n\r\n<p>Just the same, bookkeepers play a vital role in the practice because they:</p>\r\n\r\n<ul>\r\n	<li>Have access to client data 24/7</li>\r\n	<li>Acquire valuable data and insights</li>\r\n	<li>Are already providing management reporting</li>\r\n	<li>Use technology to shift to an advisory-style business</li>\r\n</ul>\r\n', 11, '1519703999'),
(151, '110981300506886', '<p>The Outsourced Accountant is the expert in offshoring solutions for Accounting and Finance firms in Australia, the U.S.A. and New Zealand. Headquartered with an executive team in Australia, we provide dedicated team members as a high-quality offshore team. Growing rapidly with over 500+ qualified accountants and support staff in the Philippines, we deliver all the recruitment, onboarding, facilities and amazing culture to support your workflow. If you have capacity restraints, are looking to scale your firm or just want to know what is possible - call our industry specialists Cam, Shane, or Forster today on 1300 896 522, or visit www.theoutsourcedaccountant.com.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Company details</strong></p>\r\n\r\n<p>Website</p>\r\n\r\n<p><a href="http://www.theoutsourcedaccountant.com/" id="ember3676" rel="noopener noreferrer" target="_blank">http://www.theoutsourcedaccountant.com</a></p>\r\n\r\n<p><strong>Headquarters</strong></p>\r\n\r\n<p>Angeles City, Pampanga</p>\r\n\r\n<p><strong>Year founded</strong></p>\r\n\r\n<p>2013</p>\r\n\r\n<p><strong>Company type</strong></p>\r\n\r\n<p>Privately Held</p>\r\n\r\n<p><strong>Company size</strong></p>\r\n\r\n<p>501-1,000 employees</p>\r\n\r\n<p><strong>Specialties</strong></p>\r\n\r\n<p>Accounting, Bookkeeping, Outsourcing, Offshoring, Marketing, and Administration and Client Services</p>\r\n', 11, '1519703941'),
(172, '110623498363600', '<p>The outsourced accountant !</p>\r\n', 10, '1519788660'),
(178, '1520557060302', '<p>asdasdasdasdasd</p>\r\n', 11, '1520557082');

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE `employee_info` (
  `ID` int(11) NOT NULL,
  `employee_id_no` text NOT NULL,
  `first_name` text NOT NULL,
  `middle_name` text NOT NULL,
  `last_name` text NOT NULL,
  `gender` text NOT NULL,
  `civil_status` text NOT NULL,
  `date_of_birth` text NOT NULL,
  `contact` text NOT NULL,
  `permanent_address` text NOT NULL,
  `current_address` text NOT NULL,
  `email` text NOT NULL,
  `department` text NOT NULL,
  `client_name` text NOT NULL,
  `segment` text NOT NULL,
  `department_id` text NOT NULL,
  `location` text NOT NULL,
  `site` text NOT NULL,
  `job_code` text NOT NULL,
  `job_title` text NOT NULL,
  `job_level_grade` text NOT NULL,
  `payroll_type` text NOT NULL,
  `employee_type` text NOT NULL,
  `employment_status` text NOT NULL,
  `hr_status` text NOT NULL,
  `hire_date` text NOT NULL,
  `regularization_date` text NOT NULL,
  `separation_date` text NOT NULL,
  `general_reason` text NOT NULL,
  `specific_reason` text NOT NULL,
  `voluntary_involuntary` text NOT NULL,
  `biometric_id` text NOT NULL,
  `reports_to_employee_id_no` text NOT NULL,
  `second_level_supervisor` text NOT NULL,
  `billability` text NOT NULL,
  `employee_remarks` text NOT NULL,
  `schedule_type` text NOT NULL,
  `payroll_pie_id` text NOT NULL,
  `highest_education_attainment` text NOT NULL,
  `college_degree` text NOT NULL,
  `major` text NOT NULL,
  `institution` text NOT NULL,
  `prior_work_experience` text NOT NULL,
  `previous_employer` text NOT NULL,
  `type_of_industry` text NOT NULL,
  `prc_license_no` text NOT NULL,
  `sss_no` text NOT NULL,
  `tin_no` text NOT NULL,
  `philhealth_no` text NOT NULL,
  `pag_ibig_no` text NOT NULL,
  `passport_no` text NOT NULL,
  `tax_status` text NOT NULL,
  `local_trunk_line` text NOT NULL,
  `local_trunk_line_pin` text NOT NULL,
  `skype_id` text NOT NULL,
  `emergency_contact_name` text NOT NULL,
  `emergency_contact_no` text NOT NULL,
  `emergency_contact_relationship` text NOT NULL,
  `emergency_contact_address` text NOT NULL,
  `bank_name` text NOT NULL,
  `bank_account_no` text NOT NULL,
  `basic_salary` text NOT NULL,
  `deminimis` text NOT NULL,
  `transportation_allowance` text NOT NULL,
  `travel_allowance` text NOT NULL,
  `other_allowance` text NOT NULL,
  `user_level` text NOT NULL,
  `password` text NOT NULL,
  `user_status` int(11) NOT NULL,
  `user_role` text NOT NULL,
  `profile_photo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_info`
--

INSERT INTO `employee_info` (`ID`, `employee_id_no`, `first_name`, `middle_name`, `last_name`, `gender`, `civil_status`, `date_of_birth`, `contact`, `permanent_address`, `current_address`, `email`, `department`, `client_name`, `segment`, `department_id`, `location`, `site`, `job_code`, `job_title`, `job_level_grade`, `payroll_type`, `employee_type`, `employment_status`, `hr_status`, `hire_date`, `regularization_date`, `separation_date`, `general_reason`, `specific_reason`, `voluntary_involuntary`, `biometric_id`, `reports_to_employee_id_no`, `second_level_supervisor`, `billability`, `employee_remarks`, `schedule_type`, `payroll_pie_id`, `highest_education_attainment`, `college_degree`, `major`, `institution`, `prior_work_experience`, `previous_employer`, `type_of_industry`, `prc_license_no`, `sss_no`, `tin_no`, `philhealth_no`, `pag_ibig_no`, `passport_no`, `tax_status`, `local_trunk_line`, `local_trunk_line_pin`, `skype_id`, `emergency_contact_name`, `emergency_contact_no`, `emergency_contact_relationship`, `emergency_contact_address`, `bank_name`, `bank_account_no`, `basic_salary`, `deminimis`, `transportation_allowance`, `travel_allowance`, `other_allowance`, `user_level`, `password`, `user_status`, `user_role`, `profile_photo`) VALUES
(10, 'qq', 'Lawrence', 'M.', 'Guiao', '', 'Single', '1520265600', 'wer234534', '534534534', '53453', 'lawrence.guiao@toa.com.ph', '45345', '', '', '', '', '', '', 'IT manager', '', '', '', '345345345', '', '1520265600', '0', '0', '', '', '', '3453453453', '345345345', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '34534534534', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '3', 'b3360cc45c2819fc1ea9b0f16c15fdee', 0, '', ''),
(11, 'dfsd', 'Emerson', 'A', 'Dimacutac', 'Male', 'Married', '512150400', 'asdas', 'Employee ID No', 'Employee ID No', 'emerson.dimacutac@theoutsourcedaccountant.ph', '234', '', '', '', '', '', '', 'Web Developer', '', '', '', 'qweqwe', '', '1520265600', '0', '0', '', '', '', '44234', 'Employee ID No', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '234234', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '4', 'b3360cc45c2819fc1ea9b0f16c15fdee', 0, '', 'image_1123_.png'),
(12, 'alejo', 'jojo', 'dsad', 'dsadsa', 'Male', 'Single', '1521561600', 'dsad', 'sad', 'sad', 'carredsonalejo@yahoo.com', 'dsad', 'sad', 'sads', 'adsads', 'dsad', 'adsa', 'dsad', 'sadas', 'sadsa', 'dsa', 'dsadas', 'adasdsa', 'xz', '1521993600', '0', '0', 'xzc', 'sad', 'sadsa', 'dasdsa', 'dasdsa', 'dsa', 'dsadas', 'das', 'sd', 'sadas', 'dsad', 'asdasd', 'sadas', 'dasd', 'sadsa', 'dsadsa', 'dsa', 'dsadas', 'dsad', 'sadas', 'dada', 'dad', 'ada', 'dadad', 'dada', 'dada', 'dad', 'adada', 'adad', 'adad', 'dadad', 'ada', 'ada', 'dadadad', 'dadad', 'adadada', 'dadada', 'dadada', '1', 'b3360cc45c2819fc1ea9b0f16c15fdee', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `employee_notify`
--

CREATE TABLE `employee_notify` (
  `EID` int(11) NOT NULL,
  `EPID` text NOT NULL,
  `user_id` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_notify`
--

INSERT INTO `employee_notify` (`EID`, `EPID`, `user_id`) VALUES
(1, '374598347598374859', '10\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `events_category_tbl`
--

CREATE TABLE `events_category_tbl` (
  `ID` int(20) NOT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_category_tbl`
--

INSERT INTO `events_category_tbl` (`ID`, `name`) VALUES
(1, 'Regular Holidays'),
(2, 'Special Non Holidays'),
(3, 'Local Holidays'),
(4, 'TOA Events'),
(5, 'Industry Events'),
(6, 'PH Social Events'),
(1, 'Regular Holidays'),
(2, 'Special Non Holidays'),
(3, 'Local Holidays'),
(4, 'TOA Events'),
(5, 'Industry Events'),
(6, 'PH Social Events');

-- --------------------------------------------------------

--
-- Table structure for table `events_tbl`
--

CREATE TABLE `events_tbl` (
  `ID` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `description` text,
  `organizer` text,
  `location` text,
  `category` text,
  `date` text,
  `month` text,
  `day` text,
  `year` text,
  `datestamp` text,
  `add_by` text,
  `start_time` text,
  `end_time` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_tbl`
--

INSERT INTO `events_tbl` (`ID`, `title`, `description`, `organizer`, `location`, `category`, `date`, `month`, `day`, `year`, `datestamp`, `add_by`, `start_time`, `end_time`) VALUES
(1, 'zxcz', 'czcz', 'xzcz', '1', '2', '0', NULL, NULL, '', '20/02/2018 11:27:20', '11', 'zcz', 'xczcz'),
(2, 'TOA Anniversary', 'TOA Anniversary', 'Emerson', '1', '4', '1518019200', '02', '08', '2018', '08/02/2018 11:05:11', '11', NULL, NULL),
(3, 'asdsa', 'sdasd', 'sdasd', '1', '2', '1518019200', '02', '08', '2018', '08/02/2018 02:57:43', '11', NULL, NULL),
(4, 'Lorem Imposium', 'Lorem Imposium dsfdfgsd dsfds dsfdsfs afsadfas', 'Lorem Imposium', '1', '1', '1518451200', '02', '13', '2018', '13/02/2018 10:48:53', '11', NULL, NULL),
(5, 'select2.js multipled', 'select2.js multipled select2.js multipled select2.js multipled sdfgds dsgdsgsd sdfggs sdgsgs dsgdsgsgs sfgsg', 'select2.js multipled', '1', '1', '1518537600', '02', '14', '2018', '13/02/2018 11:19:59', '11', NULL, NULL),
(6, 'calendarform', 'calendarform', 'calendarform', '1', '2', '1517587200', '02', '14', '2018', '20/02/2018 11:48:31', '11', '12:00', '12:00'),
(7, 'Valentines Day', 'Valentines Day', 'Valentines Day', '1', '1', '1517414400', '02', '01', '2018', '19/02/2018 07:00:35', '11', NULL, NULL),
(8, 'Lorem Imposium', 'Lorem Imposium dsfdfgsd dsfds dsfdsfs afsadfas', 'Lorem Imposium', '1', '1', '1518969600', '02', '19', '2018', '19/02/2018 01:51:32', '11', NULL, NULL),
(9, 'calendarform', 'calendarform', 'calendarform', '1', '1', '1519056000', '02', '20', '2018', '20/02/2018 07:18:50', '11', NULL, NULL),
(10, 'Lorem Imposium', 'Lorem Imposium dsfdfgsd dsfds dsfdsfs afsadfas', 'Lorem Imposium', '1', '1', '1519056000', '02', '20', '2018', '20/02/2018 07:20:09', '11', NULL, NULL),
(11, 'awe', 'eaea', 'aea', '', '', '1519056000', '02', '20', '2018', '20/02/2018 07:44:07', '11', NULL, NULL),
(12, 'esf', 'da', 'asdsa', '', '', '1519056000', '02', '20', '2018', '20/02/2018 07:51:17', '11', '', ''),
(13, 'qw', 'dada', 'qw', '', '', '1519056000', '02', '20', '2018', '20/02/2018 07:53:10', '11', '', ''),
(14, 'sdfs', 'fsfs', 'fsfs', '', '', '1518537600', '02', '14', '2018', '20/02/2018 07:54:10', '11', '18:00', ''),
(15, 'ada', 'dada', 'adad', '', '', '1519056000', '02', '20', '2018', '20/02/2018 07:54:57', '11', '12:00', '12:10'),
(16, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 08:02:05', '11', '01:00', '04:00'),
(17, 'www', '2018-02-20', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:36:03', '11', 'www', '01:00'),
(18, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:41:48', '11', '01:00', '04:00'),
(19, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:27', '11', '01:00', '04:00'),
(20, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:31', '11', '01:00', '04:00'),
(21, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:32', '11', '01:00', '04:00'),
(22, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:32', '11', '01:00', '04:00'),
(23, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:32', '11', '01:00', '04:00'),
(24, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:44', '11', '01:00', '04:00'),
(25, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:45', '11', '01:00', '04:00'),
(26, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:45', '11', '01:00', '04:00'),
(27, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:45', '11', '01:00', '04:00'),
(28, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:46', '11', '01:00', '04:00'),
(29, 'www', 'www', 'www', '1', '4', '1519056000', '02', '20', '2018', '20/02/2018 09:49:50', '11', '01:00', '04:00'),
(30, 'werwe', 'rwrwe', 'ewrwe', '1', '1', '1519056000', '02', '20', '2018', '20/02/2018 10:02:35', '11', '00:00', '12:00'),
(31, 'asdas', 'ada', 'dasd', '1', '1', '1519401600', '02', '24', '2018', '20/02/2018 11:16:39', '11', '00:05', '04:00'),
(32, 'werew', 'rewrew', 'ewrew', '1', '1', '1519056000', '02', '20', '2018', '20/02/2018 11:49:49', '11', 'werew', 'ewrew');

-- --------------------------------------------------------

--
-- Table structure for table `location_tbl`
--

CREATE TABLE `location_tbl` (
  `ID` int(11) UNSIGNED NOT NULL,
  `location` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_tbl`
--

INSERT INTO `location_tbl` (`ID`, `location`) VALUES
(1, 'Clark BC7'),
(2, 'Clark BC11'),
(3, 'Manila'),
(4, 'Gold Coast'),
(1, 'Clark BC7'),
(2, 'Clark BC11'),
(3, 'Manila'),
(4, 'Gold Coast');

-- --------------------------------------------------------

--
-- Table structure for table `tagged_group`
--

CREATE TABLE `tagged_group` (
  `TID` int(11) NOT NULL,
  `postid` varchar(300) NOT NULL,
  `gid` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagged_group`
--

INSERT INTO `tagged_group` (`TID`, `postid`, `gid`) VALUES
(26, '1519108842876', '5'),
(25, '1519108842876', '2'),
(24, '1519108842876', '1'),
(29, '1518994788971', '1'),
(30, '1518994788971', '5'),
(57, '1519700192773', '5'),
(36, '1518499641919', '1'),
(58, '1519703569606', '1'),
(35, '1518501833680', '1'),
(40, '1518407837326', '1'),
(44, '1519168024431', '1'),
(59, '1519703569606', '5'),
(46, '1519168101396', '2'),
(47, '1519168101396', '5'),
(48, '1519168672954', '2'),
(49, '1519172950149', '1'),
(50, '1519177122273', '5'),
(51, '1518058154740', '1'),
(56, '1519700192773', '1'),
(53, '1519270146115', '1'),
(54, '1519195347167', '1'),
(55, '1519195347167', '5'),
(60, '1519703945584', '1'),
(61, '1519703945584', '5'),
(62, '1519788453151', '1'),
(63, '1519788453151', '5'),
(64, '10', '2'),
(65, '10', '2'),
(66, '1519703945584', '6'),
(67, '1519779720205', '6'),
(68, '1519779720205', '1'),
(69, '110623498363600', '1'),
(70, '1519881908683', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tag_group`
--

CREATE TABLE `tag_group` (
  `ID` int(11) NOT NULL,
  `group_id` text NOT NULL,
  `user_id` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_group`
--

INSERT INTO `tag_group` (`ID`, `group_id`, `user_id`) VALUES
(2, '1', '11'),
(1, '1', '10'),
(3, '5', '10'),
(4, '6', '12'),
(5, '6', '10'),
(6, '5', '11'),
(7, '5', '12'),
(8, '1', '12');

-- --------------------------------------------------------

--
-- Table structure for table `upload_photos`
--

CREATE TABLE `upload_photos` (
  `PID` int(11) NOT NULL,
  `photo_show` text NOT NULL,
  `photo_name` varchar(300) NOT NULL,
  `photo_folder` varchar(500) NOT NULL,
  `photo_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload_photos`
--

INSERT INTO `upload_photos` (`PID`, `photo_show`, `photo_name`, `photo_folder`, `photo_user`) VALUES
(287, '1519168024431', '4471520564769.jpg', '1519168024431', 11),
(286, '1519168024431', '7121520564769.jpg', '1519168024431', 11),
(284, '1519168024431', '8601520564762.jpg', '1519168024431', 11),
(302, '1519703945584', '2791520565813.jpg', '1519703945584', 11),
(301, '1519703945584', '3321520565813.jpg', '1519703945584', 11),
(300, '1519703945584', '9631520565813.jpg', '1519703945584', 11),
(267, '1520556615105', '2851520556636.jpg', '1520556615105', 11),
(266, '1520556615105', '3651520556636.jpg', '1520556615105', 11),
(230, '1517980850904', '5901517980902.jpg', '1517980850904', 11),
(265, '1518407837326', '5141520556436.jpg', '1518407837326', 11),
(228, '110526681171633', '4111517980757.jpg', '110526681171633', 11),
(285, '1519168024431', '8521520564769.jpg', '1519168024431', 11),
(296, '1519703945584', '7691520565767.jpg', '1519703945584', 11),
(254, '1518407837326', '2551518407848.jpg', '1518407837326', 11),
(253, '1518407837326', '9901518407848.jpg', '1518407837326', 11),
(215, '1517974376584', '2161517974383.jpg', '1517974376584', 11),
(252, '1518407837326', '6101518407848.jpg', '1518407837326', 11),
(213, '109463873046298', '6301517973336.jpg', '109463873046298', 11),
(144, '110791224544269', '8941517205531.jpg', '110791224544269', 11);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `GID` int(11) NOT NULL,
  `group_name` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`GID`, `group_name`) VALUES
(1, 'IT Team'),
(2, 'HR Team'),
(5, 'Support Team'),
(6, 'Admin Team');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `VID` int(11) NOT NULL,
  `video_url` varchar(300) NOT NULL,
  `post_id` varchar(300) NOT NULL,
  `user_id` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`VID`, `video_url`, `post_id`, `user_id`) VALUES
(66, 'https://www.youtube.com/watch?v=cVQz8DNkAwM', '110623498363600', '10'),
(40, 'https://www.youtube.com/watch?v=kX9BUmKx5p8&list=RDMMkX9BUmKx5p8', '109463873046298', '11'),
(3, 'https://www.youtube.com/watch?v=amSRNwMp2nc', '1517294248890', '11'),
(68, 'https://www.youtube.com/watch?v=siufl6AIBWI', '1519703945584', '11'),
(22, 'EX:https://www.youtube.com/watch?v=cVQz8DNkAwM&feature=', '109757187244636', '11'),
(23, 'EX:https://www.youtube.com/watch?v=cVQz8DNkAwM&feature=', '109757187244636', '11'),
(29, 'https://www.youtube.com/watch?v=9pe0_Qj_PXY', '1517443380595', '11'),
(42, 'https://www.youtube.com/watch?v=kX9BUmKx5p8&list=RDMMkX9BUmKx5p8', '109028358882362', '11'),
(43, 'https://www.youtube.com/watch?v=11rPDIxRz9M&list=RDMMkX9BUmKx5p8&index=8', '1517975468338', '11'),
(44, 'https://www.youtube.com/watch?v=11rPDIxRz9M&list=RDMMkX9BUmKx5p8&index=8', '1517975468338', '11'),
(45, 'https://www.youtube.com/watch?v=11rPDIxRz9M&list=RDMMkX9BUmKx5p8&index=8', '1517975468338', '11'),
(46, 'https://www.youtube.com/watch?v=11rPDIxRz9M&list=RDMMkX9BUmKx5p8&index=8', '1517975518308', '11'),
(47, 'https://www.youtube.com/watch?v=30bYxPnOz0Q&index=9&list=RDMMkX9BUmKx5p8', '1517976081740', '11'),
(48, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=10&list=RDMMkX9BUmKx5p8', '1517976180878', '11'),
(49, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976320241', '11'),
(50, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976351641', '11'),
(51, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976370263', '11'),
(52, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976399337', '11'),
(53, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976441213', '11'),
(54, 'https://www.youtube.com/watch?v=KPugwrTFxKs&index=', '1517976497688', '11'),
(62, 'https://vimeo.com/254605418', '1518130456483', '11'),
(63, 'https://vimeo.com/255669409', '1518994788971', '11'),
(65, 'https://vimeo.com/256685249', '1519172950149', '11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`PID`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `employee_notify`
--
ALTER TABLE `employee_notify`
  ADD PRIMARY KEY (`EID`);

--
-- Indexes for table `events_category_tbl`
--
ALTER TABLE `events_category_tbl`
  ADD KEY `ID` (`ID`),
  ADD KEY `ID_2` (`ID`);

--
-- Indexes for table `events_tbl`
--
ALTER TABLE `events_tbl`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `location_tbl`
--
ALTER TABLE `location_tbl`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `tagged_group`
--
ALTER TABLE `tagged_group`
  ADD PRIMARY KEY (`TID`);

--
-- Indexes for table `tag_group`
--
ALTER TABLE `tag_group`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `upload_photos`
--
ALTER TABLE `upload_photos`
  ADD PRIMARY KEY (`PID`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`GID`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`VID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `employee_notify`
--
ALTER TABLE `employee_notify`
  MODIFY `EID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events_category_tbl`
--
ALTER TABLE `events_category_tbl`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `events_tbl`
--
ALTER TABLE `events_tbl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `location_tbl`
--
ALTER TABLE `location_tbl`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tagged_group`
--
ALTER TABLE `tagged_group`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tag_group`
--
ALTER TABLE `tag_group`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `upload_photos`
--
ALTER TABLE `upload_photos`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `GID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `VID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
