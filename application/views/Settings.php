<?php $postid = strtotime('now') . rand(100, 999) ?>
<?php
$user = $this->Models->employee_info($this->uri->segment(3));
$info = $user->result();
?>
<div class="col-5">
   <div class="panel">
      <div class="panel-heading">TEAM GROUP LIST</div>
      <div class="panel-body">
         <div class="divTable team-group-list">
            <div class="divTableHeading">
               <div class="divTableRow">
                  <div class="divTableCell">Group Name</div>
                  <div class="divTableCell text-center">Action</div>
               </div>
               <form class="divTableRow table-group-list">
                  <div class="divTableCell"><input type="text" name="group_name" class="form-control form-control-sm add-team-group"></div>
                  <div class="divTableCell text-center"><button type="submit" class="btn btn-info btn-sm submit-button"><i class="fa fa-plus"></i> Add</button></div>
               </form>
            </div>
            <div class="divTableBody form-group-list">
               <?php
               $data = $this->Models->tag_group_list();
               $group = $data->result();
               foreach ($group as $value) {
                   ?>
                   <form class="divTableRow table-group-list">
                      <input type="hidden" class="hide" name="GID" value="<?php echo $value->GID ?>">
                      <div class="divTableCell"><input type="text" class="form-control form-control-sm" name="group_name" value="<?php echo $value->group_name ?>"></div>
                      <div class="divTableCell text-center"><button type="submit" class="btn btn-success btn-sm submit-button"><i class="fas fa-pencil-alt"></i> Update</button> <button type="button" class="btn btn-danger btn-sm delete-team-group" data-value="<?php echo $value->GID ?>"><i class="fa fa-times"></i> Delete</button></div>
                   </form>
                   <?php
               }
               ?>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="col-4">

  
</div>

