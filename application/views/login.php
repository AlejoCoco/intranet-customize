<div class="container">
   <div class="login-container">
      <div class="row">
         <div class="col-6">
            <img src="<?php echo site_url('images/TOA-logo.png') ?>" class="toa-logo"/>
         </div>
         <div class="col-6">
            
            <form class="form-forgot-password forget-pass hide" method="post">
               <div class="form-group text-center member-login">
                  Forgot Password
               </div>
               <div class="form-group">
                  <div class="label-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <input type="text" class="form-login" name="email_address" placeholder="Email">
               </div>
               <div class="form-group">
                  <button class="btn btn-primary btn-lg login-btn"><span class="loader-spinner"></span><span class="text-login">SUBMIT</span></button>
               </div>
               <div class="form-group text-center">
                  <a href="#forgot" class="login-back">Back to login</a>
               </div>
               <div class="output">
               </div>
            </form>
            
            <form class="form-validation login" method="post">
               <div class="form-group text-center member-login">
                  Member Login
               </div>
               <div class="form-group">
                  <div class="label-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <input type="text" class="form-login" name="email_address" placeholder="Email">
               </div>
               <div class="form-group">
                  <div class="label-icon"><i class="fa fa-key" aria-hidden="true"></i></div>
                  <input type="password" class="form-login" name="user_password" placeholder="Password">
               </div>
               <div class="form-group">
                  <button class="btn btn-primary btn-lg login-btn"><span class="loader-spinner"></span><span class="text-login">LOGIN</span></button>
               </div>
               <div class="form-group text-center">
                  <a href="#forgot" class="login-link">Forgot Password</a>
               </div>
               <div class="output">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>


