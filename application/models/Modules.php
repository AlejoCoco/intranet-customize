<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        header('Access-Control-Allow-Origin: *');
    }

    function index() {
      
    }
    
    
    function add_tagged_group($data) {
        return $this->db->insert('tag_group', $data);
    }

    function delete_tagged_group($data) {
        $this->db->where($data);
        return $this->db->delete('tag_group');
    }
    
    function delete_profile($id) {
        $this->db->where('ID', $id);
        $this->db->delete('employee_info');
    }
    function delete_calendar($id) {
        $this->db->where('ID', $id);
        $this->db->delete('events_tbl');
    }
    function pagination($limit, $pages, $total) {

        $set_val = $limit;
        $i_value = floor($total / $set_val);
        $i_remainder = $total % $set_val;
        if ($i_remainder != 0) {
            $total_value_loop = $i_value + 1;
        } else {
            $total_value_loop = $i_value;
        }

        $prev = $pages - 1;
        $next = $pages + 1;

        $to = $pages * $set_val;
        $from = $to - $set_val + 1;


        if (strlen($pages) == "" || $pages == 0 || $pages == 1) {
            $prev = "";
        } else {
            $prev = " <a href='" . site_url("Users/page/" . $prev) . "'>&laquo;</a>";
        }
        $page_number = "";
        for ($x = 1; $x <= $total_value_loop; $x++) {

            if ($x == $pages) {
                $class = "class='active'";
            } else {
                $class = "class=''";
            }

            $page_number = $page_number . "<a href='" . site_url('Users/page/' . $x) . "' " . $class . ">" . $x . "</a>";
        }

        if ($total_value_loop == $pages) {
            $next = "";
        } else {
            $next = "<a href='" . base_url("Users/page/" . $next) . "'>&raquo;</a>";
        }
        $bind_pagination = $prev . $page_number . $next;
        return $bind_pagination;
    }
    function calendar_caption($data){
                    $label ="";
                    if($data == '1'){
                        $label ="January";
                    }elseif($data == '2'){
                        $label ="February";
                    }elseif($data == '3'){
                        $label ="March";
                    }elseif($data == '4'){
                        $label ="April";
                    }elseif($data == '5'){
                        $label ="May";
                    }elseif($data == '6'){
                        $label ="June";
                    }elseif($data == '7'){
                        $label ="July";
                    }elseif($data == '8'){
                        $label ="August";
                    }elseif($data == '9'){
                        $label ="September";
                    }elseif($data == '10'){
                        $label ="October";
                    }elseif($data == '11'){
                        $label ="November";
                    }elseif($data == '12'){
                        $label ="December";
                    }else{
                         $label="";
                    }
                    return $label;
    }
    function get_color($data){
            if($data == '1'){
                        $color ="#1abc9c";
            }elseif($data == '2'){
                        $color ="#498db";
            }elseif($data == '3'){
                        $color ="#9b59b6";
            }elseif($data == '4'){
                        $color ="#5c6270";
            }elseif($data == '5'){
                        $color ="#e67e22";
            }elseif($data == '6'){
                        $color ="#E9D460";
            }else{
                        $color ="";
            }
            return $color;
    }
    function get_events_per_month($month , $year) {
        return $this->db->query("SELECT * FROM events_tbl where month='$month' and year='$year' order by day");
    }
    function login_script($data) {
        return $this->db->query("SELECT * FROM `employee_info` WHERE email ='" . $data['email'] . "' and password='" . $data['password'] . "' ");
    }
    function events_json() {
        return $this->db->query("SELECT category as name, CONCAT(year , '-', month, '-', day) as date  FROM events_tbl");
    }

    function sessions($data) {
        $sql = "SELECT * FROM `employee_info` WHERE email ='" . $data['email'] . "' and password='" . $data['password'] . "' ";
        $query = $this->db->query($sql);
        return $query;
    }

    function model_update($id, $object) {
        $this->db->where('ID', $id);
        $this->db->update('employee_info', $object);
    }

    function model_delete_employee($id) {
        $this->db->where('ID', $id);
        $this->db->delete('employee_info');
    }

    function model_registration($data) {
        return $this->db->insert('employee_info', $data);
    }

    function add_events($data) {
        return $this->db->insert('events_tbl', $data);
    }
    function update_events($id, $object) {
         $this->db->where('ID', $id);
         $this->db->update('events_tbl', $object);
    }

    function model_get_id() {
        return $this->db->query('SELECT ID FROM employee_info ORDER BY `employee_info`.`ID` DESC LIMIT 1');
    }

    function user_info() {
        // $setLimit = $this->max_row();
        // $srh = explode(" ", $this->keyword());
        // $pageLimit = ($this->page() * $setLimit) - ($setLimit);
        //$conditions = array();
        // foreach ($srh as $field) {
//            if ($field) {
//                $conditions[] = "`employee_id_no` LIKE '%" . $field . "%'";
//                $conditions[] = "`first_name` LIKE '%" . $field . "%'";
//                $conditions[] = "`last_name` LIKE '%" . $field . "%'";
//                $conditions[] = "`email` LIKE '%" . $field . "%'";
//            }
//        }
//        $_query = "SELECT * FROM employee_info ";
//        if (count($conditions) > 0) {
//            $_query .= "WHERE " . implode(' || ', $conditions);
//        }
//        return $this->db->query($_query . " ORDER BY `last_name` LIMIT $pageLimit , $setLimit");
        return $this->db->query('SELECT ID FROM employee_info ORDER BY `employee_info`.`ID` DESC LIMIT 1');
    }

    function user_list() {
        return $this->db->query('SELECT * FROM employee_info ');
    }

    function user_info_list($filter) {
        return $this->db->query("SELECT * FROM employee_info where ID='" . $filter . "'");
    }
     function user_tag($filter) {
        return $this->db->query(" SELECT * FROM tag_group as a LEFT JOIN(SELECT * FROM user_group) as b ON a.group_id = b.GID where user_id='" . $filter . "' and LENGTH(group_name) != 0");
    }

     function calendar_info($filter) {
        return $this->db->query("SELECT * FROM events_tbl where ID='" . $filter . "'");
    }

    function events_category() {
        return $query = $this->db->query("SELECT * FROM events_category_tbl");
    }

    function location() {
        return $query = $this->db->query("SELECT * FROM location_tbl");
    }

    function count_users() {
        return $query = $this->db->query("SELECT count(*) as count FROM employee_info ");
    }

    function user_list_w_page($val, $val2, $val3) {
        return $this->db->query("
            SELECT * FROM (select @rownum:=@rownum+1 nowef, 
            p.* from employee_info p, (SELECT @rownum:=0) r ) MY_data
            where nowef BETWEEN '" . $val . "' AND '" . $val2 . "' and first_name LIKE'%" . $val3 . "%'
                or
                 last_name LIKE'%" . $val3 . "%'
                or
                 middle_name LIKE'%" . $val3 . "%'
                or
                 email LIKE'%" . $val3 . "%'
                or
                 department LIKE'%" . $val3 . "%'
             ");
    }

    function get_events($month, $year) {
        return $this->db->query("SELECT * FROM events_tbl where   month='".$month."' and year='".$year."'");
    }
    function password_validation($id, $password) {
        return $this->db->query("SELECT * FROM employee_info where   ID='".$id."' and password='".$password."'");
    }
    function get_event_today() {
        $day   =    date("d"); 
        $month =    date("m"); 
        $year  =    date("Y"); 
        
        return $this->db->query("SELECT * FROM events_tbl where  month='$month ' and year='$year' ORDER BY day ASC ");
    }
    function get_event_today_manual($month,  $year) {  
        $query = $this->db->query("SELECT * FROM events_tbl where  month='$month' and year='$year' ORDER BY day ASC ");

          return $query->result();
    }
}
