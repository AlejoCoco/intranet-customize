<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class Request extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('ImageResize');
        $this->load->model('Modules');
        $this->load->model('Models');
        $this->load->library('session');
        $this->load->database();
    }
    function like() {
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "uid" => $this->session->userdata('user_session'),
                $this->input->post('status') => '1',
                "postid" => $this->input->post('type'),
            );
            $this->Models->delete_like($this->input->post('type'));
            if ($this->input->post('status') != 'delete') {
                $this->Models->insert_like($data);
            }
            $likeme = $this->Models->check_like('like_post', $this->input->post('type'));
            $dislikeme = $this->Models->check_like('dislike', $this->input->post('type'));

            $_data = array("like" => $likeme->num_rows(), "dislike" => $dislikeme->num_rows(), "test" => $this->input->post('type'), "likename" => $this->Models->like_conversion($likeme->result()), "dislikename" => $this->Models->like_conversion($dislikeme->result()));
        } else {
            $_data = array("status" => 'error');
        }
        print json_encode($_data);
    }

    function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");
        $data = explode(',', $base64_string);
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);
        return $output_file;
    }

    function profile_picture() {
        $this->form_validation->set_rules('base64', 'base64', 'required');
        if ($this->form_validation->run()) {
            $rand = rand(10,99);
            $file = './uploads/image_' . $this->session->userdata('user_session') .$rand. '_.png';
            $file_name = 'image_' . $this->session->userdata('user_session') .$rand. '_.png';
            $empl = $this->Models->employee_info($this->session->userdata('user_session'));
            $imgp = $empl->result();
            if ($imgp[0]->profile_photo) {
                $unlink = './uploads/' . $imgp[0]->profile_photo;
                if (file_exists($unlink)) {
                    unlink($unlink);
                }
            }
            $this->base64_to_jpeg($this->input->post('base64'), $file);
            $img = imagecreatefrompng($file);
            $img_w = imagesx($img);
            $img_h = imagesy($img);
            $newPicture = imagecreatetruecolor($img_w, $img_h);
            imagesavealpha($newPicture, true);
            $color = imagecolorallocatealpha($newPicture, 0, 0, 0, 127);
            imagefill($newPicture, 0, 0, $color);
            $color = imagecolorat($img, $img_w - 1, 1);

            for ($x = 0; $x < $img_w; $x++) {
                for ($y = 0; $y < $img_h; $y++) {
                    $c = imagecolorat($img, $x, $y);
                    if ($color != $c) {
                        imagesetpixel($newPicture, $x, $y, $c);
                    }
                }
            }
            imagepng($newPicture, $file);
            imagedestroy($newPicture);
            imagedestroy($img);
            $data = array("profile_photo" => $file_name);
            $this->Modules->model_update($this->session->userdata('user_session'), $data);
            $response = array(
                "status" => 'success',
                "image" => $file_name,
                "uid" => $this->session->userdata('user_session')
            );
        } else {
            $response = array(
                "status" => 'error'
            );
        }
        print json_encode($response);
    }

    function bithday() {
        $bday = $this->Models->employee();
        $data = $bday->result();
        $array = array();
        foreach ($data as $value) {
            if (date('m', $value->date_of_birth) === date('m')) {
                echo date('m', $value->date_of_birth);
            }
        }
    }

    function index() {
        echo site_url();
    }

    function tagging() {
        $this->form_validation->set_rules('get_id', 'get_id', 'required');
        $this->form_validation->set_rules('group_id', 'group_id', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        if ($this->form_validation->run()) {
            if ($this->input->post('type') == 'add') {
                $data = array(
                    "postid" => $this->input->post('get_id'),
                    "gid" => $this->input->post('group_id'),
                );
                $this->Models->add_tagged_group($data);
                $value = array("message" => "success", "text" => "Success add");
            } else if ($this->input->post('type') == 'remove') {
                $data = array(
                    "postid" => $this->input->post('get_id'),
                    "gid" => $this->input->post('group_id'),
                );
                $this->Models->delete_tagged_group($data);
                $value = array("message" => "success", "text" => "Success remove");
            } else {
                $value = array("message" => "error", "text" => "error no tyle");
            }
        } else {
            $value = array("message" => "error", "text" => "error validation");
        }
        print json_encode($value);
    }

    function tag_group_option() {
        $this->form_validation->set_rules('keyword', 'keyword', 'required');
        if ($this->form_validation->run()) {
            $gp = $this->Models->tag_group($this->input->post('keyword'));
            print json_encode($gp->result());
        } else {
            print json_encode('');
        }
    }

    function out_tagged_group() {
        $this->form_validation->set_rules('postid', 'postid', 'required');
        if ($this->form_validation->run()) {
            $gp = $this->Models->tagged_group($this->input->post('postid'));
            print json_encode($gp->result());
        }
    }

    function adding_tagged_group() {
        $this->form_validation->set_rules('get_id', 'post_id', 'required');
        $this->form_validation->set_rules('get_id', 'post_id', 'required');
        $this->form_validation->set_rules('post_announcement', 'post_announcement', 'required');
        if ($this->form_validation->run()) {
            
        }
    }

    function weather() {
        //http://freegeoip.net/json/58.69.29.222
        //https://ipinfo.io/58.69.29.222/json
        $ch = @curl_init("http://freegeoip.net/json/" . $this->input->ip_address());
        //$ch = @curl_init("http://freegeoip.net/json/58.69.29.222");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($ch);
        echo $html;
        curl_close($ch);
    }

    function upload_img($upload) {
        return (isset($_FILES[$upload]) ? $_FILES[$upload] : '');
    }

    function attached() {
        //https://github.com/eventviva/php-image-resize
        if ($this->upload_img('upload')) {
            $key_id = strtotime("NOW") . rand(10, 99);
            $fname = $this->upload_img('upload')['name'];
            $tmpFilePath = $this->upload_img('upload')['tmp_name'];
            move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
            $this->ImageResize->addfile('./uploads/' . $key_id . '_' . $fname);
            $this->ImageResize->crop(200, 200, ImageResize::CROPCENTER);
            $this->ImageResize->save(('./uploads/200x200_' . $key_id . '_' . $fname));
            $value = array("uploaded" => "1", "fileName" => "$fname", "thumb" => site_url('uploads/200x200_' . $key_id . '_' . $fname), "url" => site_url('uploads/' . $key_id . '_' . $fname));
            print json_encode($value);
        }
    }

    function announcement() {
        $this->form_validation->set_rules('post_id', 'post_id', 'required|numeric');
        $this->form_validation->set_rules('post_announcement', 'post_announcement', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "postid" => $this->input->post('post_id'),
                "post" => $this->input->post('post_announcement'),
                "userid" => $this->session->userdata('user_session'),
                "date" => strtotime('now')
            );
            $this->Models->post_announcement($data);
            $value = array("message" => "success", "text" => "done", "post_id" => $this->input->post('post_id'));
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function multiple() {
        $this->form_validation->set_rules('get_upload_id', 'get_upload_id', 'required');
        if ($this->form_validation->run()) {
            if (!is_dir('uploads/' . $this->input->post('get_upload_id'))) {
                mkdir('uploads/' . $this->input->post('get_upload_id'));
            }
            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("jpg", "jpeg", "JPG", "png", "gif");
                $arry = array();
                $rry = array();
                for ($i = 0; $i < $total; $i++) {
                    $fname = rand('0', '1000') . strtotime('now') . '.' . pathinfo($_FILES['file']['name'][$i], PATHINFO_EXTENSION);
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $this->input->post('get_upload_id') . '/' . $fname);
                        $this->ImageResize->addfile('uploads/' . $this->input->post('get_upload_id') . '/' . $fname);
                        $this->ImageResize->crop(200, 200, ImageResize::CROPCENTER);
                        $this->ImageResize->save(('uploads/' . $this->input->post('get_upload_id') . '/' . '200x200_' . $fname));
                        $data = array(
                            "photo_show" => $this->input->post('get_upload_id'),
                            "photo_name" => $fname,
                            "photo_folder" => $this->input->post('get_upload_id'),
                            "photo_user" => $this->session->userdata('user_session')
                        );
                        $this->Models->save_image_sql($data);
                        $arry[] .= site_url('uploads/' . $this->input->post('get_upload_id') . '/' . '200x200_' . $fname);
                        $rry[] .= site_url('uploads/' . $this->input->post('get_upload_id') . '/' . $fname);
                    }
                }
                $value = array("message" => "success", "text" => "done", $arry, $rry, "document" => $this->input->post('get_upload_id'));
            } else {
                $value = array("message" => "error", "text" => "error.");
            }
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function json_posted_messege() {
        header('Content-Type: text/html; charset=utf-8');
        $this->form_validation->set_rules('get_id', 'get_id', 'required|numeric');
        if ($this->form_validation->run()) {
            $data = $this->Models->posted_announcement_id($this->input->post('get_id'));
            $post = $data->result();
            $value = array("message" => "success", "text" => $post[0]->post);
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function render_video_page() {
        header('Content-Type: text/html; charset=utf-8');
        $this->form_validation->set_rules('posted_videos_id', 'posted_videos_id', 'required');
        if ($this->form_validation->run()) {
            $data_video = $this->Models->posted_videos_id($this->input->post('posted_videos_id'));
            $post_video = $data_video->result();
            $post_num = $data_video->num_rows();
            if ($post_num > 0) {
                ?>
                <div class="col-12 video-output" >
                   <?php
                   foreach ($post_video as $video) {
                       echo $this->Models->getVideoDetails($video->video_url);
                   }
                   ?>
                </div>
                <?php
            }
        }
    }

    function render_video() {
        $this->form_validation->set_rules('posted_videos_id', 'posted_videos_id', 'required');
        if ($this->form_validation->run()) {
            $data_video = $this->Models->posted_videos_id($this->input->post('posted_videos_id'));
            print json_encode($data_video->result());
        }
    }

    function add_new_video() {
        $this->form_validation->set_rules('video_url', 'video_url', 'required');
        $this->form_validation->set_rules('get_upload_id', 'get_upload_id', 'required');
        if ($this->form_validation->run()) {
            $data = array(
                "video_url" => $this->input->post('video_url'),
                "post_id" => $this->input->post('get_upload_id'),
                "user_id" => $this->session->userdata('user_session'),
            );
            $this->Models->add_videos($data);
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }

        print json_encode($value);
    }

    function remove_video() {
        $this->form_validation->set_rules('get_upload_id', 'get_upload_id', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_videos($this->input->post('get_upload_id'));
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function remove_photo() {
        $this->form_validation->set_rules('get_upload_id', 'get_upload_id', 'required');
        $this->form_validation->set_rules('upload_name', 'upload_name', 'required');
        $this->form_validation->set_rules('upload_thumb', 'upload_thumb', 'required');
        if ($this->form_validation->run()) {

            $this->Models->delete_photo($this->input->post('get_upload_id'));
//            unlink($this->input->post('upload_name'));
//            unlink($this->input->post('upload_thumb'));
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function update_anno() {
        $this->form_validation->set_rules('get_id', 'get_id', 'required');
        $this->form_validation->set_rules('update_announcement', 'update_announcement', 'required');
        if ($this->form_validation->run()) {

            $data = array("post" => $this->input->post('update_announcement'));
            $this->Models->update_videos($this->input->post('get_id'), $data);
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }

    function delete_announcement() {
        $this->form_validation->set_rules('get_id', 'get_id', 'required');
        if ($this->form_validation->run()) {
            $this->Models->delete_announcement_id($this->input->post('get_id'));
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);
    }
    function update_group_list(){
        $this->form_validation->set_rules('group_name', 'group_name', 'required');
        if ($this->form_validation->run()) {
            if(is_numeric($this->input->post('GID'))){
                $this->Models->update_group(array("group_name"=>$this->input->post('group_name')),$this->input->post('GID'));
                $value = array("message" => "success", "text" => "success","type" => "update");
            }else{
                $this->Models->add_group(array("group_name"=>$this->input->post('group_name')));
                $value = array("message" => "success", "text" => "success","type" => "add");
            }
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);  
    }
    function delete_group_list(){
        $this->form_validation->set_rules('delete_tag', 'delete_tag', 'required|numeric');
        if ($this->form_validation->run()) {
            $this->Models->delete_group($this->input->post('delete_tag'));
            $value = array("message" => "success", "text" => "success");
        } else {
            $value = array("message" => "error", "text" => "error");
        }
        print json_encode($value);  
    }
    function tag_group_list_ajax(){
        $data = $this->Models->tag_group_list_limit();
        print json_encode($data->result());  
    }
}
